import React, { Component } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import { createTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { Switch, Route, Redirect } from "react-router-dom";
import IpfsRouter from "ipfs-react-router";

import i18n from "./i18n";
import interestTheme from "./theme";

import APR from "./components/apr";
import InvestSimple from "./components/investSimple";
import Manage from "./components/manage";
import Performance from "./components/performance";
import Zap from "./components/exchanges/zap";
import Dexs from "./components/exchanges/dexs";
import IDai from "./components/idai";
import Footer from "./components/footer";
import Header from "./components/header";
import Vaults from "./components/vault";
import Construccion from "./components/construccion";
import Dashboard from "./components/dashboard";
import Uniswap from "./components/exchanges/uniswap";
import Lending from "./components/lending";
import Home from "./components/home";
import SEO from "./components/seo";

import { injected } from "./stores/connectors";

import { CONNECTION_CONNECTED } from "./constants";

import Store from "./stores";
import { getIp } from "./services/index";
import Coins from "./components/coins";
import BridgeFiatBtc from "./components/bridges/BridgeFiatBtc/BridgeFiatBtc";
import BridgeFiatEth from "./components/bridges/BridgeFiatEth/BridgeFiatEth";
import BridgeEthBtc from "./components/bridges/BridgeEthBtc/BridgeEthBtc";
import BridgeFiatNft from "./components/bridges/BridgeFiatNft/BridgeFiatNft";
import RenBridge from "./components/bridges/dexs/RenBridge/RenBridge";
import BridgeFiatStablecoins from "./components/bridges/cexs/BridgeFiatStablecoins/BridgeFiatStablecoins";
import ListProduct from "./components/links/list-product/ListProduct";
import DefiTools from "./components/links/defi-tools/DefiTools";
import BridgeFiatCryptoGlobal from "./components/bridges/BridgeFiatCryptoGlobal/BridgeFiatCryptoGlobal";
import LightningNetTools from "./components/links/lightning-net-tools/LightningNetTools";
import AxieScholarship from "./components/links/pay-earn/axie-scholarship/AxieScholarship";
import Plays from "./components/links/pay-earn/plays/Plays";
import AxieTools from "./components/links/pay-earn/axie-tools/AxieTools";
import NewsCriptos from "./components/links/news-criptos/NewsCriptos";
import BtcPay from "./components/btcpay/BtcPay";

//import FormContact from "./components/users/formContact/FormContact";
const emitter = Store.emitter;
const store = Store.store;

class App extends Component {
  _ref = React.createRef();

  state = {
    countryName: "",
    countryCode: "",
    currency: "",
    languages: "",
    user: null,
  };

  componentWillMount() {
    injected.isAuthorized().then((isAuthorized) => {
      if (isAuthorized) {
        injected
          .activate()
          .then((a) => {
            store.setStore({
              account: { address: a.account },
              web3context: { library: { provider: a.provider } },
            });
            emitter.emit(CONNECTION_CONNECTED);
          })
          .catch((e) => {
            console.log(e);
          });
      } else {
      }
    });

    if (window.ethereum) {
      window.ethereum.on("accountsChanged", function (accounts) {
        store.setStore({ account: { address: accounts[0] } });

        const web3context = store.getStore("web3context");
        if (web3context) {
          emitter.emit(CONNECTION_CONNECTED);
        }
      });
    }
  }

  componentDidMount() {
    getIp()
      .then((data) => {
        const lan = data.languages.split(",", 1)[0];
        this.setState({
          countryName: data.country_name,
          countryCode: data.country,
          currency: data.currency,
          languages: lan,
        });
        const cur = data.currency;
        const local = lan.split("-", 1)[0];
        console.log(local);
        const inner = {
          symbols: [
            {
              proName: "BITSTAMP:BTCUSD",
              title: "BTC/USD",
            },
            {
              proName: "BITSTAMP:ETHUSD",
              title: "ETH/USD",
            },
            {
              description: cur === "USD" ? "EUR/USD" : cur + "/USD",
              proName: cur === "USD" ? "FX:EURUSD" : "FX_IDC:USD" + cur,
            },
          ],
          showSymbolLogo: true,
          colorTheme: "light",
          isTransparent: false,
          displayMode: "adaptive",
          locale: local,
        };

        const script = document.createElement("script");
        script.src =
          "https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js";
        script.async = true;
        script.innerHTML = JSON.stringify(inner);
        this._ref.current.appendChild(script);
        i18n.changeLanguage(this.state.languages.split("-", 1)[0]);
      })
      .catch((error) => {
        console.log(error.message); // 'An error has occurred: 404'
      });
  }

  render() {
    return (
      <MuiThemeProvider theme={createTheme(interestTheme)}>
        <CssBaseline />
        <IpfsRouter>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              minHeight: "100vh",
              alignItems: "center",
              background: "#f9fafb",
            }}
          >
            <SEO />
            <div style={{ width: "100%" }}>
              <div className="tradingview-widget-container" ref={this._ref}>
                <div className="tradingview-widget-container__widget"></div>
              </div>
            </div>
            <Switch>
              <Route path="/coins">
                <Header />
                <Coins />
              </Route>
              <Route path="/links-interes/list">
                <Header />
                <ListProduct />
              </Route>
              <Route path="/links-interes/defi-tools">
                <Header />
                <DefiTools />
              </Route>
              <Route path="/links-interes/lightning-tools">
                <Header />
                <LightningNetTools />
              </Route>
              <Route path="/play-to-earn/axie-scholarship">
                <Header />
                <AxieScholarship />
              </Route>
              <Route path="/play-to-earn/plays">
                <Header />
                <Plays />
              </Route>
              <Route path="/play-to-earn/axie-tools">
                <Header />
                <AxieTools />
              </Route>
              <Route path="/links-interes/news-criptos">
                <Header />
                <NewsCriptos />
              </Route>
              {/* <Route path="/contact-us">
                <Header />
                <FormContact />
              </Route> */}
              <Route path="/bridges/fiat-btc">
                <Header />
                <BridgeFiatBtc />
              </Route>
              <Route path="/bridges/fiat-eth">
                <Header />
                <BridgeFiatEth />
              </Route>
              <Route path="/bridges/eth-btc">
                <Header />
                <BridgeEthBtc />
              </Route>
              <Route path="/:type/fiat-ntf">
                <Header />
                <BridgeFiatNft />
              </Route>
              <Route path="/btc-pay/:price">
                <Header />
                <BtcPay />
              </Route>
              <Route path="/dexs/ren-bridge">
                <Header />
                <RenBridge />
              </Route>
              <Route path="/cexs/fiat-crypto-local">
                <Header />
                <BridgeFiatStablecoins />
              </Route>
              <Route path="/cexs/fiat-crypto-global">
                <Header />
                <BridgeFiatCryptoGlobal />
              </Route>
              <Route path="/stats">
                <Header />
                <APR />
              </Route>
              <Route path="/earn">
                <Header />
                <InvestSimple />
              </Route>
              <Route path="/exchanges/dexs">
                <Header />
                <Dexs />
              </Route>
              <Route path="/exchanges/zap">
                <Header />
                <Zap />
              </Route>
              <Route path="/exchanges/tuswap">
                <Header />
                <Uniswap />
              </Route>
              <Route path="/lending">
                <Header />
                <Lending />
              </Route>
              <Route path="/idai">
                <IDai />
              </Route>
              <Route path="/performance">
                <Header />
                <Performance />
              </Route>
              <Route path="/manage">
                <Header />
                <Manage />
              </Route>
              <Route path="/home">
                <Header />
                <Home />
              </Route>
              <Route path="/vaults">
                <Header />
                <Vaults />
              </Route>
              <Route path="/construccion">
                <Header />
                <Construccion />
              </Route>
              <Route path="/dashboard">
                <Header />
                <Dashboard />
              </Route>

              <Route path="/">
                {/* { <Home /> } */}
                {<Redirect from="/" to="/Home" />}
              </Route>
            </Switch>
            <Footer />
          </div>
        </IpfsRouter>
      </MuiThemeProvider>
    );
  }
}

export default App;
