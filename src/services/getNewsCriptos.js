import axios from "axios";
import varEnv from "../config/varEnv";

export async function getNewsCriptos(param) {
  const locale = param.locale === "es" ? param.locale : "en";
  const url = varEnv.urlNewsCriptos + locale;
  //console.log(param);
  try {
    const data = await await axios.get(`${url}`);
    // console.log(data);
    return data;
  } catch (error) {
    console.log("ERROR: " + error);
    return [];
  }
}
