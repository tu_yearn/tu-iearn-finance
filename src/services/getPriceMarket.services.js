import varEnv from "../config/varEnv";
import axios from "axios";

export async function getPriceMarket(currency) {
  const url = varEnv.urlLocalbitcoins + "/price-market/" + currency;
  try {
    return (await axios.get(url)).data;
  } catch (error) {
    console.log("ERROR: " + error);
    return [];
  }
}
