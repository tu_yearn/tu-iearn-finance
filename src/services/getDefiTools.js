import axios from "axios";
import varEnv from "../config/varEnv";

export async function getDefiTools(param) {
  const url = varEnv.urlDefiTools;
  //console.log(param);
  try {
    const data = await await axios.get(`${url}`);
    // console.log(data);
    return data;
  } catch (error) {
    console.log("ERROR: " + error);
    return [];
  }
}
