import axios from "axios";

export async function getAdsByCurrency(
  urlProvider,
  trader,
  currency,
  PaymentMethods
) {
  if (trader === "" || currency === "" || PaymentMethods === "") {
    console.log("Falta parametros");
    return [];
  }
  const url = `${urlProvider}/currency/${trader}/${currency}/${PaymentMethods}`;
  try {
    return (await axios.get(url)).data;
  } catch (error) {
    console.log("ERROR: " + error);
    return [];
  }
}
