import axios from "axios";
import varEnv from "../config/varEnv";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

function alertNoData(alert, type) {
  Toast.fire({
    icon: type,
    timer: 5000,
    title: alert,
    width: "350px",
  });
}

export  async function getDefaultPerfil(param) {
  const url = varEnv.urlUserWalletGet+"/"+param;
  try {
    const data = await (await axios.get(url)).data;

    if (data.message === "Consulta existosa") {
      console.log(`${data.message}: datos Wallet Usuario...!`);
    } else if(data.statusCode===404){
      console.log(`Datos: ${data.message}...!`);     
    }
    return data;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status === 400) {
        alertNoData(error.response.data.message, "error");
      }
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log("error1 "+error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error2 ", error.message);
    }
    console.log(error.config);
    return [];
  }
}
export async function postPerfil(param) {
  const url = varEnv.urlCreateUserPost;
  console.log(param);
  try {
    const data = await (await axios.post(url, param)).data;
    console.log(data);
    const count = data.totalCount;
   if (count === 0) {
      //alertNoData("Sin data", "info");
    } else {
      /*alertNoData(
        "Los datos se han enviado satisfactoriamente, pronto lo contactaremos",
        "info"
      );*/
    }
    return data;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status === 400) {
        alertNoData(error.response.data.message, "error");
      }
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return [];
  }
}

export async function validateSendMail(param) {
  //const url = varEnv.urlUserSubscribePost;
  //console.log(param);
  try {
    const data = await (await axios.get("https://jsonplaceholder.typicode.com/posts")).data;
    //console.log(data);
    //alert(param.firstname);
    //const count = data.totalCount;
    if (param === 1) {
      return data;
      //alertNoData("Sin data "+data, "info");
    } /*else {
      data=false;*/
     /* alertNoData(
        "Los datos se han enviado satisfactoriamente, pronto lo contactaremos",
        "info"
      );*/
   // }
    return data;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status === 400) {
        alertNoData(error.response.data.message, "error");
      }
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return [];
  }
}

export async function validateExistsEmail(param) {
  const url = varEnv.urlUserMailGet+"/"+param;
  
  console.log("**param** "+param);
  console.log("**url** "+url);
  try {
    const data = await (await axios.get(url)).data;
    console.log("**data** "+data);
    if (data.message === "successful query") {
      console.log(`${data.message}: datos Mail Usuario...!`);
    } else{
      console.log(`Datos: ${data.message}...!`);     
    }
    return data;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status === 400) {
        alertNoData(error.response.data.message, "error");
      }
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return [];
  }
}
