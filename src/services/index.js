export { getIp } from "./getIp.services";
export { getAdsByCountry } from "./getAdsByCountry";
export {
  getProvidersByCountry,
  getProvidersByCurrency,
  getProvidersByPaymentMethods,
} from "./getProviders";
export { getAdsByCurrency } from "./getAdsByCurrency";
export { getAdsByPaymentMethods } from "./getAdsByPaymentMethods";
export { getPriceMarket } from "./getPriceMarket.services";
export { getCoingeckoMarkets } from "./getCoingeckoMarkets";
export { getAds } from "./getAds";
export { getPaymentMethods } from "./getPaymentMethods";
export { postContactUs } from "./postContactUs";
export { getStableCoinsCexs } from "./getStableCoinsCexs";
export { postPerfil } from "./perfilServices";
export { getExchagesDexs } from "./getExchagesDexs";
export { getBridgeFiatNft } from "./getBridgeFiatNft";
export { getDefiTools } from "./getDefiTools";
export { getExchangeCexs } from "./getExchangeCexs";
export { getLightningNetTools } from "./getLightningNetTools";
export { getAxieScholarship } from "./getAxieScholarship";
export { getPlays } from "./getPlays";
export { getAxieTools } from "./getAxieTools";
export { getNewsCriptos } from "./getNewsCriptos";
