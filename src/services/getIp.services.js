import varEnv from "../config/varEnv";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

function alertNoData() {
  Toast.fire({
    icon: "info",
    timer: 10000,
    width: 500,
    title:
      "Para brindarle una mejor experiencia con TUFi, se requiere que por favor desactive los escudos globales para permitir acceso a su geolocalización.\n\n To give you a better TUFi experience, it is required that you please disable global shields to allow access to your geolocation.",
  });
}

export async function getIp() {
  try {
    const response = await fetch(varEnv.urlGetIp);
    // tryGetJson(response).then((data) => console.log(data));

    if (response.ok) {
      console.log("IP ok");
    } else {
      const ip = {
        country: "VE",
        country_code: "VE",
        currency: "VES",
        languages: "es-VE",
      };
      const message = `An error has occured: ${response.status}`;
      console.log(message);
      // console.log(ip);
      alertNoData();
      // throw new Error(message);
      return ip;
    }

    const ip = await response.json();
    // console.log(ip);
    return ip;
  } catch (error) {
    console.log(`A real error!: ${error}`);
    alertNoData();
    const ip = {
      country: "VE",
      country_code: "VE",
      currency: "VES",
      languages: "es-VE",
    };
    console.log(ip);
    return ip;
  }
}

/* const tryGetJson = async (resp) => {
  return await new Promise((resolve) => {
    if (resp) {
      resp
        .json()
        .then((json) => resolve(json))
        .catch(() => resolve(null));
    } else {
      resolve(null);
    }
  });
}; */
