import axios from "axios";
import varEnv from "../config/varEnv";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

function alertNoData(alert) {
  Toast.fire({
    icon: "info",
    timer: 5000,
    title: alert,
  });
}

export async function getStableCoinsCexs(param, alert) {
  const url = varEnv.urlStableCoinsCexs;
  try {
    const res = await axios.get(
      `${url}?page=${param.page}&limit=${param.limit}&filter=${param.filter}`
    );
    const count = res.data.totalCount;
    //console.log(count);
    if (!count || count === 0) {
      alertNoData(alert);
    }
    return res.data;
  } catch (error) {
    console.log("ERROR: " + error);
    alertNoData("Error interno en el servidor");
    return [];
  }
}
