import axios from "axios";

export async function getAdsByCountry(
  urlProvider,
  trader,
  countryCode,
  country
) {
  if (trader === "" || countryCode === "" || country === "") {
    console.log("Falta parametros");
    return [];
  }
  const url = `${urlProvider}/country/${trader}/${countryCode}/${country}`;
  try {
    return (await axios.get(url)).data;
  } catch (error) {
    console.log("ERROR: " + error);
    return [];
  }
}
