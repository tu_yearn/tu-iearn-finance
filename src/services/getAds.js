import axios from "axios";
import varEnv from "../config/varEnv";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

function alertNoData(alert) {
  Toast.fire({
    icon: "info",
    timer: 5000,
    title: alert,
  });
}

export async function getAds(param, alert) {
  const url = varEnv.urlAds;
  // console.log(param);
  try {
    const data = await (
      await axios.get(
        `${url}?page=${param.page}&limit=${param.limit}&sortOrderName=${
          param.sortOrderName
        }&sortOrderDirection=${param.sortOrderDirection.toUpperCase()}&filter=${
          param.filter
        }&fiat_currency=${param.currency}&coin_currency=${
          param.coin_currency
        }&countrycode=${param.countrycode}&online_provider=${
          param.online_provider
        }&textSearch=${param.textSearch}`
      )
    ).data;
    //console.log(data);
    const count = data.totalCount;
    if (count === 0) {
      alertNoData(alert);
    }
    return data;
  } catch (error) {
    console.log("ERROR: " + error);
    return [];
  }
}
