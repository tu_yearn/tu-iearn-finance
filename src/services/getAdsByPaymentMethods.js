import axios from "axios";

export async function getAdsByPaymentMethods(
  urlProvider,
  trader,
  countryCode,
  country,
  PaymentMethods
) {
  if (
    trader === "" ||
    countryCode === "" ||
    country === "" ||
    PaymentMethods === ""
  ) {
    console.log("Falta parametros");
    return [];
  }
  const url = `${urlProvider}/country/${trader}/${countryCode}/${country}/${PaymentMethods}`;
  try {
    return (await axios.get(url)).data;
  } catch (error) {
    console.log("ERROR: " + error);
    return [];
  }
}
