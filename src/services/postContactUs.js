import axios from "axios";
import varEnv from "../config/varEnv";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

function alertNoData(alert, type) {
  Toast.fire({
    icon: type,
    timer: 5000,
    title: alert,
    width: "350px",
  });
}

export async function postContactUs(param) {
  const url = varEnv.urlUserSubscribePost;
  // console.log(param);
  try {
    const data = await (await axios.post(url, param)).data;
    console.log(data);
    const count = data.totalCount;
    if (count === 0) {
      alertNoData("Sin data", "info");
    } else {
      alertNoData(
        "Los datos se han enviado satisfactoriamente, pronto lo contactaremos",
        "info"
      );
    }
    return data;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status === 400) {
        alertNoData(error.response.data.message, "error");
      }
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return [];
  }
}
