import axios from "axios";
import varEnv from "../config/varEnv";

export async function getPaymentMethods() {
  const url = varEnv.urlPaymentMethods;
  try {
    return (await axios.get(url)).data;
  } catch (error) {
    console.log("ERROR: " + error);
    return [];
  }
}
