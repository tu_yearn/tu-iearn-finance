import axios from "axios";
import varEnv from "../config/varEnv";

export async function getExchagesDexs(param) {
  const url = varEnv.urlExchagesDexs;
  // console.log(param);
  try {
    const data = await (
      await axios.get(
        `${url}?page=${param.page}&limit=${param.limit}&sortOrderName=${
          param.sortOrderName
        }&sortOrderDirection=${param.sortOrderDirection.toUpperCase()}&filter=${
          param.filter
        }&textSearch=${param.textSearch}`
      )
    ).data;
    // console.log(data);
    return data;
  } catch (error) {
    console.log("ERROR: " + error);
    return [];
  }
}
