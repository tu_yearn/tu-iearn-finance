import { getAdsByCountry } from "./getAdsByCountry";
import { getAdsByCurrency } from "./getAdsByCurrency";
import { getAdsByPaymentMethods } from "./getAdsByPaymentMethods";

import varEnv from "../config/varEnv";

export function getProvidersByCountry(countryCode, country) {
  const promises = [
    getAdsByCountry(varEnv.urlLocalbitcoins, "buy", countryCode, country),
    getAdsByCountry(varEnv.urlLocalbitcoins, "sell", countryCode, country),
    getAdsByCountry(varEnv.urlLocalCoinSwap, "buy_sell", countryCode, country),
  ];
  return promises;
}

export function getProvidersByCurrency(currency, PaymentMethods) {
  const promises = [
    getAdsByCurrency(varEnv.urlLocalbitcoins, "buy", currency, PaymentMethods),
    getAdsByCurrency(varEnv.urlLocalbitcoins, "sell", currency, PaymentMethods),
    getAdsByCurrency(
      varEnv.urlLocalCoinSwap,
      "buy_sell",
      currency,
      PaymentMethods
    ),
  ];
  return promises;
}

export function getProvidersByPaymentMethods(
  countryCode,
  country,
  PaymentMethods
) {
  const promises = [
    getAdsByPaymentMethods(
      varEnv.urlLocalbitcoins,
      "buy",
      countryCode,
      country,
      PaymentMethods
    ),
    getAdsByPaymentMethods(
      varEnv.urlLocalbitcoins,
      "sell",
      countryCode,
      country,
      PaymentMethods
    ),
    getAdsByPaymentMethods(
      varEnv.urlLocalCoinSwap,
      "buy_sell",
      countryCode,
      country,
      PaymentMethods
    ),
  ];
  return promises;
}
