import React, { Component, createRef, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { withNamespaces } from "react-i18next";
import { withStyles } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import i18n from "../../../i18n";
import {
  Button,
  CircularProgress,
  FormControl,
  FormGroup,
  FormLabel,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import { createTheme, MuiThemeProvider } from "@material-ui/core/styles";
import moment from "moment";
import "moment/locale/es";
import styles from "./styles";
import * as data from "./data";
import { getPaymentMethods, getAds, getIp } from "../../../services/index";
import clsx from "clsx";

//const SPACED_DATE_FORMAT = "DD/MM/YYYY h:mm a";

class BridgeFiatBtc extends Component {
  constructor(props) {
    super(props);
    this.tableRef = createRef();
    this.currencyData = data.currencyData;
    this.comboCurrency = data.comboCurrency;
  }
  alertNoData = this.props.t("Alert.NoData");
  loadData = this.props.t("Alert.loadData");
  state = {
    isLoading: false,
    value: 1,
    selectedCurrency: "",
    selectedCountry: "",
    selectedPaymentMethods: "ALL_ONLINE",
    paymentMethodsData: [],
    page: 0,
    count: 1,
    rowsPerPage: 50,
    sortOrder: {},
    data: [[this.loadData]],
    priceFilterChecked: false,
    filterSelect: "",
    textSearchFilter: "",
    selectSortOrderName: "",
    changeTrade: this.props.t("Bridge.FiatToBtc"),
  };

  headerTable = () => {
    return {
      className: clsx({
        [this.props.classes.BoldCell]: true,
      }),
      style: {
        textDecoration: "underline",
      },
    };
  };

  convertToCurrency = (value) => {
    const nf = new Intl.NumberFormat(this.state.languages, {
      style: "currency",
      currency:
        this.state.selectedCurrency !== ""
          ? this.state.selectedCurrency
          : "VES",
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    });
    if (isNaN(value)) {
      return "";
    }
    return nf.format(value);
  };

  getMuiTheme = () =>
    createTheme({
      overrides: {
        MUIDataTable: {
          root: {
            backgroundColor: "inherit",
          },
          paper: {
            boxShadow: "none",
          },
        },
        MUIDataTableBodyCell: {
          root: {
            backgroundColor: "none",
          },
        },
      },
    });

  paymentMethods() {
    getPaymentMethods().then((res) => {
      // console.log(res);
      this.setState({
        paymentMethodsData: res.data,
      });
    });
  }

  componentDidMount() {
    getIp()
      .then((data) => {
        // console.log(data);
        const country = data.country_code;
        const currency = data.currency;
        const lang =
          data.languages === "" || data.languages === undefined
            ? "es-VE"
            : data.languages;
        this.setState({
          selectedCountry: country,
          selectedCurrency: currency,
          languages: lang.split(",", 1),
        });
        i18n.changeLanguage(lang.split("-", 1)[0]);
        this.setState({ isLoading: true });
        const { rowsPerPage } = this.state;
        const param = {
          page: "1",
          limit: rowsPerPage,
          sortOrderName: "temp_price",
          sortOrderDirection: "ASC",
          filter: "trade_type='buy'",
          currency: currency,
          countrycode: country,
          online_provider: "",
          textSearch: "",
          coin_currency: "BTC",
        };
        getAds(param, this.alertNoData).then((res) => {
          // console.log(res);
          this.setState({
            data: res.data,
            isLoading: false,
            count: res.totalCount,
          });
        });
      })
      .catch((error) => {
        console.log(error.message); // 'An error has occurred: 404'
      });
    this.paymentMethods();
  }

  componentWillUnmount() {
    this.setState = (state, callback) => {
      return;
    };
  }

  renderData = async (value) => {
    const { t } = this.props;
    this.setState({
      isLoading: true,
      data: [[this.loadData]],
      changeTrade: value === 1 ? t("Bridge.FiatToBtc") : t("Bridge.BtcToFiat"),
    });
    const trans = value === 1 ? "buy" : "sell";
    const fil = `trade_type='${trans}'`;
    const {
      rowsPerPage,
      selectedCountry,
      selectedCurrency,
      selectedPaymentMethods,
      textSearchFilter,
      sortOrder,
    } = this.state;

    const param = {
      page: 1,
      limit: rowsPerPage,
      sortOrderName: sortOrder.name ? sortOrder.name : "temp_price",
      sortOrderDirection: sortOrder.direction
        ? sortOrder.direction
        : value === 1
        ? "ASC"
        : "DESC",
      filter: fil,
      currency: selectedCurrency,
      countrycode: selectedCountry,
      textSearch: textSearchFilter,
      online_provider:
        selectedPaymentMethods === "ALL_ONLINE" ? "" : selectedPaymentMethods,
      coin_currency: "BTC",
    };
    getAds(param, this.alertNoData).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        filterSelect: fil,
        page: res.page - 1,
      });
    });
  };

  setCurrency = async (cur) => {
    this.setState({
      isLoading: true,
      selectedCurrency: cur,
      data: [[this.loadData]],
    });
    const {
      rowsPerPage,
      selectedCountry,
      selectedPaymentMethods,
      value,
      textSearchFilter,
      sortOrder,
    } = this.state;
    const trans = value === 1 ? "buy" : "sell";
    const fil = `trade_type='${trans}'`;

    const param = {
      page: 1,
      limit: rowsPerPage,
      sortOrderName: sortOrder.name ? sortOrder.name : "temp_price",
      sortOrderDirection: sortOrder.direction
        ? sortOrder.direction
        : value === 1
        ? "ASC"
        : "DESC",
      filter: fil,
      currency: cur,
      countrycode: selectedCountry,
      textSearch: textSearchFilter,
      online_provider:
        selectedPaymentMethods === "ALL_ONLINE" ? "" : selectedPaymentMethods,
      coin_currency: "BTC",
    };
    getAds(param, this.alertNoData).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        filterSelect: fil,
        page: res.page - 1,
      });
    });
  };

  setCountry = async (country) => {
    this.setState({
      isLoading: true,
      selectedCountry: country,
      data: [[this.loadData]],
    });
    const {
      rowsPerPage,
      selectedCurrency,
      selectedPaymentMethods,
      value,
      textSearchFilter,
      sortOrder,
    } = this.state;
    const trans = value === 1 ? "buy" : "sell";
    const fil = `trade_type='${trans}'`;

    const param = {
      page: 1,
      limit: rowsPerPage,
      sortOrderName: sortOrder.name ? sortOrder.name : "temp_price",
      sortOrderDirection: sortOrder.direction
        ? sortOrder.direction
        : value === 1
        ? "ASC"
        : "DESC",
      filter: fil,
      currency: selectedCurrency,
      countrycode: country,
      textSearch: textSearchFilter,
      online_provider:
        selectedPaymentMethods === "ALL_ONLINE" ? "" : selectedPaymentMethods,
      coin_currency: "BTC",
    };
    getAds(param, this.alertNoData).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        filterSelect: fil,
        page: res.page - 1,
      });
    });
  };

  setPaymentMethods = async (payment) => {
    this.setState({
      isLoading: true,
      selectedPaymentMethods: payment,
      data: [[this.loadData]],
    });
    const {
      rowsPerPage,
      selectedCurrency,
      selectedCountry,
      value,
      textSearchFilter,
      sortOrder,
    } = this.state;
    const trans = value === 1 ? "buy" : "sell";
    const fil = `trade_type='${trans}'`;

    const param = {
      page: 1,
      limit: rowsPerPage,
      sortOrderName: sortOrder.name ? sortOrder.name : "temp_price",
      sortOrderDirection: sortOrder.direction
        ? sortOrder.direction
        : value === 1
        ? "ASC"
        : "DESC",
      filter: fil,
      currency: selectedCurrency,
      countrycode: selectedCountry,
      online_provider: payment === "ALL_ONLINE" ? "" : payment,
      textSearch: textSearchFilter,
      coin_currency: "BTC",
    };
    getAds(param, this.alertNoData).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        filterSelect: fil,
        page: res.page - 1,
      });
    });
  };

  handleTabChange = (event, newValue) => {
    this.setState({ value: newValue });
    this.renderData(newValue);
  };

  sort = (page, sortOrder) => {
    this.setState({ isLoading: true, sortOrder, data: [[this.loadData]] });
    const {
      rowsPerPage,
      filterSelect,
      value,
      selectedCountry,
      selectedCurrency,
      selectedPaymentMethods,
      textSearchFilter,
    } = this.state;
    const param = {
      page: page + 1,
      limit: rowsPerPage,
      sortOrderName: sortOrder.name ? sortOrder.name : "temp_price",
      sortOrderDirection: sortOrder.direction
        ? sortOrder.direction
        : value === 1
        ? "ASC"
        : "DESC",
      filter: filterSelect,
      currency: selectedCurrency,
      countrycode: selectedCountry,
      textSearch: textSearchFilter,
      online_provider:
        selectedPaymentMethods === "ALL_ONLINE" ? "" : selectedPaymentMethods,
      coin_currency: "BTC",
    };
    getAds(param, this.alertNoData).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        page: res.page - 1,
      });
    });
  };

  changePage = (page, sortOrder) => {
    this.setState({ isLoading: true, data: [[this.loadData]] });
    const {
      rowsPerPage,
      filterSelect,
      value,
      selectedPaymentMethods,
      selectedCountry,
      selectedCurrency,
      textSearchFilter,
    } = this.state;
    const param = {
      page: page + 1,
      limit: rowsPerPage,
      sortOrderName: sortOrder.name ? sortOrder.name : "temp_price",
      sortOrderDirection: sortOrder.direction
        ? sortOrder.direction
        : value === 1
        ? "ASC"
        : "DESC",
      filter: filterSelect,
      currency: selectedCurrency,
      countrycode: selectedCountry,
      textSearch: textSearchFilter,
      online_provider:
        selectedPaymentMethods === "ALL_ONLINE" ? "" : selectedPaymentMethods,
      coin_currency: "BTC",
    };
    getAds(param, this.alertNoData).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        page: res.page - 1,
      });
    });
  };

  changeRowsPerPage = (sortOrder, rowsPerPage) => {
    this.setState({ isLoading: true, data: [[this.loadData]] });
    const {
      filterSelect,
      value,
      selectedCountry,
      selectedCurrency,
      selectedPaymentMethods,
      textSearchFilter,
    } = this.state;
    const param = {
      page: 1,
      limit: rowsPerPage,
      sortOrderName: sortOrder.name ? sortOrder.name : "temp_price",
      sortOrderDirection: sortOrder.direction
        ? sortOrder.direction
        : value === 1
        ? "ASC"
        : "DESC",
      filter: filterSelect,
      currency: selectedCurrency,
      countrycode: selectedCountry,
      textSearch: textSearchFilter,
      online_provider:
        selectedPaymentMethods === "ALL_ONLINE" ? "" : selectedPaymentMethods,
      coin_currency: "BTC",
    };
    getAds(param, this.alertNoData).then((res) => {
      console.log(res);
      this.setState({
        page: param.page - 1,
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        rowsPerPage,
      });
    });
  };

  render() {
    const { classes, t } = this.props;

    const {
      isLoading,
      value,
      selectedCurrency,
      selectedCountry,
      selectedPaymentMethods,
      languages,
      data,
      count,
      rowsPerPage,
      sortOrder,
      paymentMethodsData,
      changeTrade,
    } = this.state;

    const columns = [
      {
        name: "name",
        label: t("Bridge.User"),
        options: {
          filter: false,
          sort: true,
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      {
        name: "temp_price",
        label: t("Bridge.Price") + " / BTC",
        options: {
          sort: true,
          setCellHeaderProps: () => this.headerTable(),
          filter: true,
          filterType: "custom",
          customFilterListOptions: {
            render: (v) =>
              this.renderCustomFilterListOptions(
                v,
                t("Bridge.Price") + " / BTC"
              ),
            update: (filterList, filterPos, index) =>
              this.updateCustomFilterListOptions(
                filterList,
                filterPos,
                index,
                columns
              ),
          },
          filterOptions: {
            names: [],
            logic(temp_price, filters) {
              if (filters[0] && filters[1]) {
                return temp_price < filters[0] || temp_price > filters[1];
              } else if (filters[0]) {
                return temp_price < filters[0];
              } else if (filters[1]) {
                return temp_price > filters[1];
              }
              return false;
            },
            display: (filterList, onChange, index, column) =>
              this.displayfilterOptions(
                filterList,
                onChange,
                index,
                column,
                t("Bridge.Price") + " / BTC"
              ),
          },
          setCellProps: () => ({ style: { textAlign: "end" } }),
          customBodyRenderLite: (value) =>
            this.convertToCurrency(data[value].temp_price),
        },
      },
      {
        name: "min_amount",
        label: t("Bridge.MinimumAmount"),
        options: {
          sort: true,
          setCellHeaderProps: () => this.headerTable(),
          filter: true,
          filterType: "custom",
          customFilterListOptions: {
            render: (v) =>
              this.renderCustomFilterListOptions(v, t("Bridge.MinimumAmount")),
            update: (filterList, filterPos, index) =>
              this.updateCustomFilterListOptions(
                filterList,
                filterPos,
                index,
                columns
              ),
          },
          filterOptions: {
            names: [],
            logic(min_amount, filters) {
              if (filters[0] && filters[1]) {
                return min_amount < filters[0] || min_amount > filters[1];
              } else if (filters[0]) {
                return min_amount < filters[0];
              } else if (filters[1]) {
                return min_amount > filters[1];
              }
              return false;
            },
            display: (filterList, onChange, index, column) =>
              this.displayfilterOptions(
                filterList,
                onChange,
                index,
                column,
                t("Bridge.MinimumAmount")
              ),
          },
          setCellProps: () => ({ style: { textAlign: "end" } }),
          customBodyRenderLite: (value) =>
            this.convertToCurrency(data[value].min_amount),
        },
      },
      {
        name: "max_amount",
        label: t("Bridge.MaximumAmount"),
        options: {
          sort: true,
          setCellHeaderProps: () => this.headerTable(),
          filter: true,
          filterType: "custom",
          customFilterListOptions: {
            render: (v) =>
              this.renderCustomFilterListOptions(v, t("Bridge.MaximumAmount")),
            update: (filterList, filterPos, index) =>
              this.updateCustomFilterListOptions(
                filterList,
                filterPos,
                index,
                columns
              ),
          },
          filterOptions: {
            names: [],
            logic(max_amount, filters) {
              if (filters[0] && filters[1]) {
                return max_amount < filters[0] || max_amount > filters[1];
              } else if (filters[0]) {
                return max_amount < filters[0];
              } else if (filters[1]) {
                return max_amount > filters[1];
              }
              return false;
            },
            display: (filterList, onChange, index, column) =>
              this.displayfilterOptions(
                filterList,
                onChange,
                index,
                column,
                t("Bridge.MaximumAmount")
              ),
          },
          setCellProps: () => ({ style: { textAlign: "end" } }),
          customBodyRenderLite: (value) =>
            this.convertToCurrency(data[value].max_amount),
        },
      },
      {
        name: "bank_name",
        label: t("Bridge.WayToPay"),
        options: {
          filter: false,
          sort: false,
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      /* {
        name: "visible",
        label: t("Bridge.Visible"),
        options: {
          filter: false,
          sort: false,
          setCellHeaderProps: () => this.headerTable(),
          display: false,
          filterList: [t("Bridge.Yes")],
          customFilterListOptions: {
            render: (v) => `${t("Bridge.Visible")}: ${v}`,
          },
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <FormControlLabel
                value={value ? t("Bridge.Yes") : t("Bridge.No")}
                disabled
                control={
                  <Checkbox
                    checked={value}
                    value={value ? t("Bridge.Yes") : t("Bridge.No")}
                  />
                }
                label={value ? t("Bridge.Yes") : t("Bridge.No")}
              />
            );
          },
        },
      }, */
      {
        name: "last_online",
        label: t("Bridge.LastOnline"),
        options: {
          filter: false,
          sort: true,
          setCellHeaderProps: () => this.headerTable(),
          customBodyRender: (value) => {
            return moment(value).startOf("minute").fromNow();
          },
        },
      },
      {
        name: "location_string",
        label: t("Bridge.Location"),
        options: {
          filter: false,
          sort: false,
          display: false,
        },
      },
      {
        name: "payment_window_minutes",
        label: t("Bridge.PaymentWindow"),
        options: {
          filter: false,
          sort: false,
          display: false,
        },
      },
      {
        name: "trade_count",
        label: t("Bridge.TradeCount"),
        options: {
          filter: false,
          sort: false,
          display: false,
        },
      },
      {
        name: "feedback_score",
        label: t("Bridge.FeedbackScore"),
        options: {
          filter: false,
          sort: false,
          display: false,
          customBodyRender: (value) => value + "%",
        },
      },
      {
        name: "msg",
        label: t("Bridge.Msg"),
        options: {
          filter: false,
          sort: false,
          display: false,
        },
      },
      {
        name: "provider",
        label: t("Bridge.Provider"),
        options: {
          filter: false,
          sort: true,
          display: false,
          viewColumns: false,
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      {
        name: "trade_type",
        label: "Tipo oferta",
        options: {
          filter: false,
          sort: false,
          display: false,
          viewColumns: false,
        },
      },
      {
        name: "online_provider",
        label: "Método Pago",
        options: {
          filter: false,
          sort: true,
          display: false,
          viewColumns: false,
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      {
        name: "public_view",
        label: t("Bridge.Link"),
        options: {
          filter: false,
          sort: true,
          setCellHeaderProps: () => this.headerTable(),
          viewColumns: false,
          customBodyRenderLite: (value) => {
            return data[value].provider ? (
              <a
                href={data[value].public_view}
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src={
                    require(`../../../assets/${data[value].provider}-logo.png`)
                      .default
                  }
                  height={"60px"}
                  width={"60px"}
                  alt={data[value].provider}
                />
              </a>
            ) : (
              ""
            );
          },
        },
      },
    ];

    const options = {
      filter: true,
      filterType: "dropdown",
      confirmFilters: true,
      // Calling the applyNewFilters parameter applies the selected filters to the table
      customFilterDialogFooter: (currentFilterList, applyNewFilters) => {
        return (
          <div style={{ marginTop: "40px" }}>
            <Button
              variant="contained"
              onClick={() => this.handleFilterSubmit(applyNewFilters, columns)}
            >
              {this.props.t("Coins.ApplyFilters")}
            </Button>
          </div>
        );
      },
      // callback that gets executed when filters are confirmed
      onFilterConfirm: (filterList) => {
        // console.log("onFilterConfirm");
        // console.dir(filterList);
      },
      onFilterDialogOpen: () => {
        // console.log("filter dialog opened");
      },
      onFilterDialogClose: () => {
        // console.log("filter dialog closed");
      },
      onFilterChange: (column, filterList, type) => {
        if (type === "chip") {
          // console.log("updating filters via chip");
        }
      },

      searchProps: {
        onBlur: (e) => {
          this.textSearch(e.target.value);
        },
        onKeyUp: (e) => {
          if (e.keyCode === 13) {
            this.textSearch(e.target.value);
          }
        },
      },

      responsive: "standard",
      serverSide: true,
      count: count,
      rowsPerPage: rowsPerPage,
      rowsPerPageOptions: [10, 50, 100, 150, 250],
      sortOrder: sortOrder,
      selectableRows: "none",
      onTableChange: (action, tableState) => {
        // console.log(action, tableState);

        // a developer could react to change on an action basis or
        // examine the state as a whole and do whatever they want

        switch (action) {
          case "changePage":
            this.changePage(tableState.page, tableState.sortOrder);
            break;
          case "sort":
            this.sort(tableState.page, tableState.sortOrder);
            break;
          case "filterChange":
            //console.log(tableState);
            break;
          case "changeRowsPerPage":
            this.changeRowsPerPage(
              tableState.sortOrder,
              tableState.rowsPerPage
            );
            break;
          case "onSearchClose":
            this.setState({ textSearchFilter: "" });
            this.textSearch("");
            break;
          default:
          // console.log("action not handled.");
        }
      },
      textLabels: {
        pagination: {
          next: t("Bridge.Next"),
          previous: t("Bridge.Previous"),
          rowsPerPage: t("Bridge.RowsPerPage"),
          displayRows: t("Bridge.DisplayRows"),
        },
        toolbar: {
          search: t("Bridge.Search"),
          downloadCsv: t("Bridge.Download") + " CSV",
          print: t("Bridge.Print"),
          viewColumns: t("Bridge.Column"),
          filterTable: t("Bridge.Filter"),
        },
        filter: {
          all: t("Bridge.Alls"),
          title: t("Bridge.Filters"),
          reset: t("Bridge.Reset"),
        },
        viewColumns: {
          title: t("Bridge.ShowColumns"),
          titleAria: t("Bridge.ShowHideColumns"),
        },
      },
    };

    moment.locale(languages);
    return (
      <Fragment>
        {isLoading && (
          <CircularProgress size={80} className={classes.loading} />
        )}
        <div className={classes.root}>
          <div className={classes.intro}>
            <ToggleButtonGroup
              value={value}
              onChange={this.handleTabChange}
              aria-label="version"
              exclusive
              size={"large"}
            >
              <ToggleButton value={1} aria-label="v2">
                <Typography variant={"h4"}>{t("Bridge.Buy")}</Typography>
              </ToggleButton>
              <ToggleButton value={2} aria-label="v3">
                <Typography variant={"h4"}>{t("Bridge.Sell")}</Typography>
              </ToggleButton>
            </ToggleButtonGroup>
          </div>

          <div className={classes.space}>
            <br />

            <FormControl>
              <InputLabel id="demo-simple-select-label">
                {t("Bridge.Currencies")}
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={selectedCurrency}
                className={classes.selectMenu}
                onChange={(e) => this.setCurrency(e.target.value)}
              >
                {this.comboCurrency.map((cur) => {
                  return (
                    <MenuItem key={cur.code} value={cur.code}>
                      {cur.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
            <FormControl>
              <InputLabel id="demo-simple-select-label">
                {t("Bridge.Country")}
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={selectedCountry}
                className={classes.selectMenu}
                onChange={(e) => this.setCountry(e.target.value)}
              >
                {this.currencyData
                  .sort(function (a, b) {
                    if (a.name > b.name) {
                      return 1;
                    }
                    if (a.name < b.name) {
                      return -1;
                    }
                    // a must be equal to b
                    return 0;
                  })
                  .map((cur) => {
                    return (
                      <MenuItem key={cur.id} value={cur.code}>
                        {cur.name}
                      </MenuItem>
                    );
                  })}
              </Select>
            </FormControl>
            <FormControl>
              <InputLabel id="demo-simple-select-label">
                {t("Bridge.PaymentMethod")}
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={selectedPaymentMethods}
                className={classes.selectMenu2}
                onChange={(e) => this.setPaymentMethods(e.target.value)}
              >
                {paymentMethodsData.map((cur) => {
                  return (
                    <MenuItem key={cur.code} value={cur.code}>
                      {cur.description}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>

            <MuiThemeProvider theme={this.getMuiTheme()}>
              <MUIDataTable
                title={
                  <div>
                    <h1 className={classes.title}>{changeTrade} P2P</h1>
                  </div>
                }
                isLoading={this.state.isLoading}
                columns={columns}
                data={data}
                options={options}
              />
            </MuiThemeProvider>
          </div>
        </div>
      </Fragment>
    );
  }

  textSearch = (text) => {
    this.setState({
      isLoading: true,
      textSearchFilter: text,
      data: [[this.loadData]],
    });
    const {
      rowsPerPage,
      filterSelect,
      value,
      selectedPaymentMethods,
      selectedCountry,
      selectedCurrency,
      sortOrder,
    } = this.state;
    const param = {
      page: 1,
      limit: rowsPerPage,
      sortOrderName: sortOrder.name ? sortOrder.name : "temp_price",
      sortOrderDirection: sortOrder.direction
        ? sortOrder.direction
        : value === 1
        ? "ASC"
        : "DESC",
      filter: filterSelect,
      currency: selectedCurrency,
      countrycode: selectedCountry,
      online_provider:
        selectedPaymentMethods === "ALL_ONLINE" ? "" : selectedPaymentMethods,
      textSearch: text,
      coin_currency: "BTC",
    };
    getAds(param, this.alertNoData).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        page: res.page - 1,
      });
    });
  };

  handleFilterSubmit = (applyFilters, columns) => {
    let filterList = applyFilters();
    const col = columns.map((elem) => elem.name);
    const fil = filterList.map((elem) => {
      if (elem[0] === "") elem[0] = undefined;
      if (elem[1] === "") elem[1] = undefined;
      if (elem[0] !== undefined && elem[1] !== undefined) {
        return `BETWEEN ${elem[0]} AND ${elem[1]}`;
      } else if (elem[0] !== undefined) {
        if (isNaN(elem[0])) {
          return `LIKE '%${elem[0]}%'`;
        } else {
          return `>= ${elem[0]}`;
        }
        //return `>= ${elem[0]}`;
      } else if (elem[1] !== undefined) {
        return `<= ${elem[1]}`;
      } else return "";
    });
    const tr = this.state.value === 1 ? "buy" : "sell";
    let cont = "";
    let i = 0;
    fil[12] = `= '${tr}'`;
    //console.log(col);
    col.forEach((element, index) => {
      if (fil[index] !== "") {
        if (i !== 0) {
          cont = cont + " AND";
        }
        cont = cont + " " + element + " " + fil[index];
        // console.log(element, fil[index]);
        i++;
      }
    });
    this.setState({
      isLoading: true,
      filterSelect: cont,
      data: [[this.loadData]],
    });
    const {
      page,
      rowsPerPage,
      selectedCountry,
      selectedCurrency,
      selectedPaymentMethods,
      value,
      textSearchFilter,
      sortOrder,
    } = this.state;
    const param = {
      page: page + 1,
      limit: rowsPerPage,
      sortOrderName: sortOrder.name ? sortOrder.name : "temp_price",
      sortOrderDirection: sortOrder.direction
        ? sortOrder.direction
        : value === 1
        ? "ASC"
        : "DESC",
      filter: cont,
      currency: selectedCurrency,
      countrycode: selectedCountry,
      online_provider:
        selectedPaymentMethods === "ALL_ONLINE" ? "" : selectedPaymentMethods,
      textSearch: textSearchFilter,
      coin_currency: "BTC",
    };
    getAds(param, this.alertNoData).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
      });
    });
  };

  renderCustomFilterListOptions = (v, label) => {
    if (v[0] && v[1] && this.state.priceFilterChecked) {
      return [`Min ${label}: ${v[0]}`, `Max ${label}: ${v[1]}`];
    } else if (v[0] && v[1] && !this.state.priceFilterChecked) {
      return `Min ${label}: ${v[0]}, Max ${label}: ${v[1]}`;
    } else if (v[0]) {
      return `Min ${label}: ${v[0]}`;
    } else if (v[1]) {
      return `Max ${label}: ${v[1]}`;
    }
    return [];
  };

  updateCustomFilterListOptions = (filterList, filterPos, index, columns) => {
    // console.log("customFilterListOnDelete: ", filterList, filterPos, index);
    let newFilters = () => filterList;

    if (filterPos === 0) {
      filterList[index].splice(filterPos, 1, "");
    } else if (filterPos === 1) {
      filterList[index].splice(filterPos, 1);
    } else if (filterPos === -1) {
      filterList[index] = [];
    }

    this.handleFilterSubmit(newFilters, columns);

    return filterList;
  };

  displayfilterOptions = (filterList, onChange, index, column, title) => {
    return (
      <div>
        <FormLabel>{title}</FormLabel>
        <FormGroup row>
          <TextField
            label="min"
            value={filterList[index][0] || ""}
            onChange={(event) => {
              filterList[index][0] = event.target.value;
              onChange(filterList[index], index, column);
            }}
            style={{ width: "45%", marginRight: "5%" }}
          />
          <TextField
            label="max"
            value={filterList[index][1] || ""}
            onChange={(event) => {
              filterList[index][1] = event.target.value;
              onChange(filterList[index], index, column);
            }}
            style={{ width: "45%" }}
          />
        </FormGroup>
      </div>
    );
  };
}

export default withNamespaces()(withRouter(withStyles(styles)(BridgeFiatBtc)));
