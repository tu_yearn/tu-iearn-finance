import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  TextField,
  InputAdornment,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@material-ui/core';
import i18n from "../../../../i18n";
import SearchIcon from '@material-ui/icons/Search';
import { withNamespaces } from 'react-i18next';
import styles from "./styles";
import Snackbar from '../../../snackbar'
import Loader from '../../../loader'
import { getStableCoinsCexs, getIp } from "../../../../services";
import * as data from "./data";

const glf = {
    name_coin: 'Glufco',
    logo_provider: require('../../../../assets/Glufco-logo.png').default,
    image: 'https://assets.coingecko.com/coins/images/11602/small/glufco.png?1593573216'
};

const ptr = {
    name_coin: 'Petro',
    logo_provider: require('../../../../assets/Criptolago-logo.png').default,
    image: 'https://vex.sunacrip.gob.ve/assets/img/ptr.svg'
}

class BridgeFiatStablecoins extends Component {

  constructor(props) {
    super(props)

    this.comboCurrency = data.comboCurrency;
    this.currencyData = data.currencyData;
    this.handleClick = this.handleClick.bind(this);

  }
  alertNoData = this.props.t("Alert.NoData");
  state = {
    data: [],
    snackbarType: null,
    snackbarMessage: null,
    search: '',
    searchError: false,
    loading: true,
    selectedCurrency: "",
    selectedCountry: ""
  }

    componentWillMount() {

        getIp()
      .then((data) => {
      //  console.log(data);
        const country = data.country_code;
        const currency = data.currency;
        const lang =
          data.languages === "" || data.languages === undefined
            ? "es-VE"
            : data.languages;
        this.setState({
          selectedCountry: country,
          selectedCurrency: currency,
          languages: lang.split(",", 1),
        });
        i18n.changeLanguage(lang.split("-", 1)[0]);

        const param = {
            page: "1",
            limit: 250,
            filter: `currency_symbol='${currency}' AND country_code='${country}'`
          };
        getStableCoinsCexs(param, this.alertNoData).then((res) => {
         //   console.log(res);
            this.setState({
              data: res.data ? res.data : [],
              loading: false,
            });
          });
    });
  }

  componentWillUnmount() {
    this.setState = (state, callback) => {
        return;
      };
  };

  render() {
    const { classes, t } = this.props;
    const {
      loading,
      snackbarMessage,
      data
    } = this.state

    return (
        <div className={ classes.root }>
            <div className={ classes.investedContainer }>
                <Typography variant={'h5'} className={ classes.disaclaimer }>{ t("Warning.Risk") }</Typography>
                { this.renderSelects() }
                { data.length > 0 ? this.renderFilters() : "" }
                { data.length > 0 ? this.renderAssetBlocks() : "" }
            </div>
            { loading && <Loader /> }
            { snackbarMessage && this.renderSnackbar() }
        </div>
    )
  };

  onSearchChanged = (event) => {
    let val = []
    val[event.target.id] = event.target.value
    this.setState(val)
  }

  onChange = (event) => {
    let val = []
    val[event.target.id] = event.target.checked
    this.setState(val)
  };

  handleClick = (url, e) => {
    console.log(e);
    window.open(url, "_blank")
};

setCurrency = async (cur) => {
    this.setState({
      isLoading: true,
      selectedCurrency: cur,
      data: [],
    });
    const param = {
        page: "1",
        limit: 250,
        filter: `currency_symbol='${cur}' AND country_code='${this.state.selectedCountry}'`
      };
    getStableCoinsCexs(param, this.alertNoData).then((res) => {
        console.log(res);
        this.setState({
          data: res.data ? res.data : [],
          loading: false,
        });
      });
}

setCountry = async (country) => {
    this.setState({
      isLoading: true,
      selectedCountry: country,
      data: [],
    });
    const param = {
        page: "1",
        limit: 250,
        filter: `currency_symbol='${this.state.selectedCurrency}' AND country_code='${country}'`
      };
    getStableCoinsCexs(param, this.alertNoData).then((res) => {
        console.log(res);
        this.setState({
          data: res.data ? res.data : [],
          loading: false,
        });
      });
}

  renderAssetBlocks = () => {
    const { search, data } = this.state
    const { classes, t } = this.props
    const width = window.innerWidth

    return data && data.filter((asset) => {

      if(search && search !== '') {
        return asset.currency_name.toLowerCase().includes(search.toLowerCase()) ||
              asset.currency_symbol.toLowerCase().includes(search.toLowerCase()) ||
              asset.provider.toLowerCase().includes(search.toLowerCase()) ||
              asset.coin.toLowerCase().includes(search.toLowerCase())

      } else {
        return true
      }
    }).map((asset) => {
        if (asset.coin === 'GLF') {
            asset.name_coin = glf.name_coin;
            asset.image = glf.image;
        } else if (asset.coin === 'PTR') {
            asset.name_coin = ptr.name_coin;
            asset.image = ptr.image;
        }

      return (
        <div className={ classes.addressContainer } key={ asset.id } onClick={(e) => this.handleClick(asset.url_provider, e)}>
            <div className={ classes.assetSummary } >
              <div className={classes.headingName}>
                <div className={ classes.assetIcon }>
                  <img
                    alt=""
                    src={ asset.image }
                    height={ width > 600 ? '40px' : '30px' }
                  />
                </div>
                <div>
                  <Typography variant={ 'h3' } className={ classes.assetName } noWrap>{ asset.coin }</Typography>
                  <Typography variant={ 'h5' } className={ classes.grey }>{ asset.name_coin }</Typography>
                </div>
              </div>
              <div className={classes.headingEarning}>
                  <Typography variant={ 'h5' } className={ classes.grey }>{ t("FiatStableCoin.Cup") }:</Typography>
                  <div className={ classes.flexy }>
                    <Typography variant={ 'h3' } noWrap>{ parseFloat(asset.tasa).toFixed(2) } ({ asset.currency_symbol })</Typography>
                    <Typography variant={ 'h5' } className={ classes.on }> { t("FiatStableCoin.On") } </Typography>
                    <Typography variant={ 'h3' } noWrap>{ asset.currency_name }</Typography>
                  </div>
              </div>
              <div className={classes.heading}>
                  <Typography variant={ 'h5' } className={ classes.grey }>Proveedor:</Typography>
                  <Typography variant={ 'h3' } noWrap>{ asset.provider }</Typography>
              </div>
              <div className={classes.provider}>
              <a
                href={ asset.url_provider }
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src={ asset.logo_provider }
                  height={"40px"}
                  width={"40px"}
                  alt={asset.provider}
                />
              </a>
              </div>


            </div>

        </div>
      )
    })
  }

  renderFilters = () => {
    const { loading, search, searchError } = this.state
    const { classes } = this.props

    return (
      <div className={ classes.filters }>
        <TextField
          fullWidth
          disabled={ loading }
          className={ classes.searchField }
          id={ 'search' }
          value={ search }
          error={ searchError }
          onChange={ this.onSearchChanged }
          placeholder="GLF, ETH, ..."
          variant="outlined"
          InputProps={{
            startAdornment: <InputAdornment position="end" className={ classes.inputAdornment }><SearchIcon /></InputAdornment>,
          }}
        />
      </div>
    )
  }

  renderSelects = () => {
    const { classes, t } = this.props;
    const { selectedCurrency, selectedCountry } = this.state;
    return <div> <FormControl>
    <InputLabel id="demo-simple-select-label">
      {t("Bridge.Currencies")}
    </InputLabel>
    <Select
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      value={selectedCurrency}
      className={classes.selectMenu}
      onChange={(e) => this.setCurrency(e.target.value)}
    >
      {this.comboCurrency.map((cur) => {
        return (
          <MenuItem key={cur.code} value={cur.code}>
            {cur.name}
          </MenuItem>
        );
      })}
    </Select>
  </FormControl>
  <FormControl>
    <InputLabel id="demo-simple-select-label">
      {t("Bridge.Country")}
    </InputLabel>
    <Select
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      value={selectedCountry}
      className={classes.selectMenu}
      onChange={(e) => this.setCountry(e.target.value)}
    >
      {this.currencyData.sort(function (a, b) {
                    if (a.name > b.name) {
                      return 1;
                    }
                    if (a.name < b.name) {
                      return -1;
                    }
                    // a must be equal to b
                    return 0;
                  }).map((cur) => {
        return (
          <MenuItem key={cur.id} value={cur.code}>
            {cur.name}
          </MenuItem>
        );
      })}
    </Select>
  </FormControl>
  </div>
  }


  renderSnackbar = () => {
    var {
      snackbarType,
      snackbarMessage
    } = this.state
    return <Snackbar type={ snackbarType } message={ snackbarMessage } open={true}/>
  };

}

export default withNamespaces()(withRouter(withStyles(styles)(BridgeFiatStablecoins)));
