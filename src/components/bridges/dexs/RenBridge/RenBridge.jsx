import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import { withNamespaces } from 'react-i18next';
import IframeResizer from 'iframe-resizer-react';

import styles from './styles'

class RenBridge extends Component {

  constructor(props) {
    super()
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={ classes.root } style={{ width: '100%', height: '800px' }}>
        <IframeResizer
            heightCalculationMethod="lowestElement"
            src="https://tufi.finance/projects/mint"
            style={{ width: '100%', height: '800px' }}
        />
      </div>
    )
  };

  nav = (screen) => {
    this.props.history.push(screen)
  }
}

export default withNamespaces()(withRouter(withStyles(styles)(RenBridge)));
