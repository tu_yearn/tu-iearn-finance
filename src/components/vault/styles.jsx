import { colors } from '../../theme'

const styles = theme => ({
    root: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      maxWidth: '1200px',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    investedContainerLoggedOut: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      minWidth: '100%',
      marginTop: '40px',
      [theme.breakpoints.up('md')]: {
        minWidth: '900px',
      }
    },
    investedContainer: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'flex-start',
      minWidth: '100%',
      marginTop: '40px',
      [theme.breakpoints.up('md')]: {
        minWidth: '900px',
      }
    },
    balancesContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      flexWrap: 'wrap',
      justifyContent: 'flex-end',
      padding: '12px 12px',
      position: 'relative',
    },
    connectContainer: {
      padding: '12px',
      display: 'flex',
      justifyContent: 'center',
      width: '100%',
      maxWidth: '450px',
      [theme.breakpoints.up('md')]: {
        width: '450',
      }
    },
    intro: {
      width: '100%',
      position: 'relative',
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      paddingBottom: '32px',
      [theme.breakpoints.down('sm')]: {
        justifyContent: 'center',
        maxWidth: 'calc(100vw - 24px)',
        flexWrap: 'wrap'
      }
    },
    introCenter: {
      maxWidth: '500px',
      textAlign: 'center',
      display: 'flex',
      padding: '24px 0px'
    },
    introText: {
      paddingLeft: '20px'
    },
    actionButton: {
      '&:hover': {
        backgroundColor: "#2F80ED",
      },
      padding: '12px',
      backgroundColor: "#2F80ED",
      border: '1px solid #E1E1E1',
      fontWeight: 500,
      [theme.breakpoints.up('md')]: {
        padding: '15px',
      }
    },
    heading: {
      display: 'none',
      flex: 1,
      [theme.breakpoints.up('md')]: {
        display: 'block'
      }
    },
    headingName: {
      display: 'flex',
      alignItems: 'center',
      width: '350px',
      [theme.breakpoints.down('sm')]: {
        width: 'auto',
        flex: 1
      }
    },
    headingEarning: {
      display: 'none',
      width: '340px',
      [theme.breakpoints.up('sm')]: {
        display: 'block'
      }
    },
    buttonText: {
      fontWeight: '700',
      color: 'white',
    },
    assetSummary: {
      display: 'flex',
      alignItems: 'center',
      flex: 1,
      flexWrap: 'wrap',
      [theme.breakpoints.up('sm')]: {
        flexWrap: 'nowrap'
      }
    },
    assetName: {
      [theme.breakpoints.down('sm')]: {
        fontSize: '14px'
      }
    },
    assetIcon: {
      display: 'flex',
      alignItems: 'center',
      verticalAlign: 'middle',
      borderRadius: '20px',
      height: '30px',
      width: '30px',
      textAlign: 'center',
      cursor: 'pointer',
      marginRight: '20px',
      [theme.breakpoints.up('sm')]: {
        height: '40px',
        width: '40px',
        marginRight: '24px',
      }
    },
    addressContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      overflow: 'hidden',
      flex: 1,
      whiteSpace: 'nowrap',
      fontSize: '0.83rem',
      textOverflow:'ellipsis',
      cursor: 'pointer',
      padding: '28px 30px',
      borderRadius: '50px',
      border: '1px solid '+colors.borderBlue,
      alignItems: 'center',
      maxWidth: '450px',
      [theme.breakpoints.up('md')]: {
        width: '100%'
      }
    },
    between: {
      width: '40px'
    },
    expansionPanel: {
      maxWidth: 'calc(100vw - 24px)',
      width: '100%'
    },
    versionToggle: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },
    tableHeadContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    investAllContainer: {
      paddingTop: '24px',
      display: 'flex',
      justifyContent: 'flex-end',
      width: '100%'
    },
    disaclaimer: {
      padding: '12px',
      border: '1px solid rgb(174, 174, 174)',
      borderRadius: '0.75rem',
      marginBottom: '24px',
      lineHeight: '1.2',
      background: colors.white,
      '& a' : {
        color: colors.black,
        textDecoration: 'none',
        fontWeight: 'bold',
      },
      '& a:hover' : {
        textDecoration: 'underline',
      },
    },
    fees: {
      paddingRight: '75px',
      padding: '12px',
      lineHeight: '1.2',
    },
    walletAddress: {
      padding: '0px 12px'
    },
    walletTitle: {
      flex: 1,
      color: colors.darkGray
    },
    grey: {
      color: colors.darkGray
    },
    filters: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      [theme.breakpoints.down('sm')]: {
        padding: '0px 12px'
      },
    },
    searchField: {
      flex: 1,
      background: colors.white,
      borderRadius: '50px'
    },
    checkbox: {
      flex: 1,
      margin: '0px !important'
    },
    flexy: {
      display: 'flex',
      alignItems: 'center'
    },
    on: {
      color: colors.darkGray,
      padding: '0px 6px'
    },
    positive: {
      color: colors.compoundGreen
    },
    basedOnContainer: {
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    infoIcon: {
      fontSize: '1em',
      marginRight: '6px'
    },
    removePadding: {
      padding: '0px',
      maxWidth: '1040px'
    }
  });

  export default styles;
