import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import {
  Grid,
  Typography,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  TextField,
  InputAdornment,
  FormControlLabel,
  Checkbox,
  Tooltip,
  MenuItem
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SearchIcon from '@material-ui/icons/Search';
import InfoIcon from '@material-ui/icons/Info';
import HelpIcon from '@material-ui/icons/Help';
import { withNamespaces } from 'react-i18next';
import styles from "./styles";
import { colors } from '../../theme'
import Snackbar from '../snackbar'
import Asset from './asset'
import Loader from '../loader'

import {
  ERROR,
  GET_VAULT_BALANCES_FULL,
  VAULT_BALANCES_FULL_RETURNED,
  DEPOSIT_VAULT_RETURNED,
  WITHDRAW_VAULT_RETURNED,
  DEPOSIT_ALL_VAULT_RETURNED,
  WITHDRAW_ALL_VAULT_RETURNED,
  CONNECTION_CONNECTED,
  CONNECTION_DISCONNECTED
} from '../../constants'

import Store from "../../stores";
const emitter = Store.emitter
const dispatcher = Store.dispatcher
const store = Store.store


class Vault extends Component {

  constructor(props) {
    super()

    const account = store.getStore('account')
    const basedOn = localStorage.getItem('yearn.finance-dashboard-basedon')
    const filterPorcent = 5;

    this.state = {
      assets: store.getStore('vaultAssets'),
      usdPrices: store.getStore('usdPrices'),
      account: account,
      address: account.address ? account.address.substring(0,6)+'...'+account.address.substring(account.address.length-4,account.address.length) : null,
      snackbarType: null,
      snackbarMessage: null,
      search: '',
      searchError: false,
      hideZero: localStorage.getItem('yearn.finance-hideZero') === '1' ? true : false,
      basedOn: basedOn ? parseInt(basedOn > 3 ? 3 : basedOn) : 1,
      filterPorcent: filterPorcent ? parseInt(filterPorcent) : 5,
      loading: true
    }

    if(account && account.address) {
      dispatcher.dispatch({ type: GET_VAULT_BALANCES_FULL, content: {} })
    }
  }
  componentWillMount() {
    emitter.on(DEPOSIT_VAULT_RETURNED, this.showHash);
    emitter.on(WITHDRAW_VAULT_RETURNED, this.showHash);
    emitter.on(DEPOSIT_ALL_VAULT_RETURNED, this.showHash);
    emitter.on(WITHDRAW_ALL_VAULT_RETURNED, this.showHash);
    emitter.on(ERROR, this.errorReturned);
    emitter.on(VAULT_BALANCES_FULL_RETURNED, this.balancesReturned);
    emitter.on(CONNECTION_CONNECTED, this.connectionConnected);
    emitter.on(CONNECTION_DISCONNECTED, this.connectionDisconnected);
  }

  componentWillUnmount() {
    emitter.removeListener(DEPOSIT_VAULT_RETURNED, this.showHash);
    emitter.removeListener(WITHDRAW_VAULT_RETURNED, this.showHash);
    emitter.removeListener(DEPOSIT_ALL_VAULT_RETURNED, this.showHash);
    emitter.removeListener(WITHDRAW_ALL_VAULT_RETURNED, this.showHash);
    emitter.removeListener(ERROR, this.errorReturned);
    emitter.removeListener(CONNECTION_CONNECTED, this.connectionConnected);
    emitter.removeListener(CONNECTION_DISCONNECTED, this.connectionDisconnected);
    emitter.removeListener(VAULT_BALANCES_FULL_RETURNED, this.balancesReturned);
  };

  balancesReturned = (balances) => {
    this.setState({
      assets: store.getStore('vaultAssets') ,
      loading: false
    })
  };

  connectionConnected = () => {
    const { t } = this.props
    const account = store.getStore('account')

    this.setState({
      loading: true,
      account: account,
      address: account.address ? account.address.substring(0,6)+'...'+account.address.substring(account.address.length-4,account.address.length) : null
    })


    dispatcher.dispatch({ type: GET_VAULT_BALANCES_FULL, content: {} })

    const that = this
    setTimeout(() => {
      const snackbarObj = { snackbarMessage: t("Unlock.WalletConnected"), snackbarType: 'Info' }
      that.setState(snackbarObj)
    })
  };

  connectionDisconnected = () => {
    this.setState({
      account: null,
      address: null
    })
  }

  errorReturned = (error) => {
    const snackbarObj = { snackbarMessage: null, snackbarType: null }
    this.setState(snackbarObj)
    this.setState({ loading: false })
    const that = this
    setTimeout(() => {
      const snackbarObj = { snackbarMessage: error.toString(), snackbarType: 'Error' }
      that.setState(snackbarObj)
    })
  };

  showHash = (txHash) => {
    const snackbarObj = { snackbarMessage: null, snackbarType: null }
    this.setState(snackbarObj)
    this.setState({ loading: false })
    const that = this
    setTimeout(() => {
      const snackbarObj = { snackbarMessage: txHash, snackbarType: 'Hash' }
      that.setState(snackbarObj)
    })
  };

  render() {
    const { classes, t } = this.props;
    const {
      loading,
      account,
      snackbarMessage,
    } = this.state

    if(!account || !account.address) {
      return (
        <div className={ classes.root }>
          <div className={ classes.investedContainerLoggedOut }>
          <Typography variant={'h5'} className={ classes.disaclaimer }>{ t("Warning.Risk") }</Typography>
            <div className={ classes.introCenter }>
              <Typography variant='h3'>{ t("Message.Conect") }</Typography>
            </div>
          </div>
          { snackbarMessage && this.renderSnackbar() }
        </div>
      )
    }

    return (
        <div className={ classes.root }>
            <div className={ classes.investedContainer }>
                <Typography variant={'h5'} className={ classes.disaclaimer }>{ t("Warning.Risk") }</Typography>
                { this.renderFilters() }
                { this.renderBasedOn() }
                { this.renderFilterPorcent() }
                { this.renderAssetBlocks() }
            </div>
            { loading && <Loader /> }
            { snackbarMessage && this.renderSnackbar() }
            <div className={ classes.construccion }>
                <h2>{ t("Vault.TitleMessage") }</h2>
                <p>{ t("Vault.Message1") }</p>
                <p>{ t("Vault.Message2") }</p>
            </div>
        </div>
    )
  };

  onSearchChanged = (event) => {
    let val = []
    val[event.target.id] = event.target.value
    this.setState(val)
  }

  onChange = (event) => {
    let val = []
    val[event.target.id] = event.target.checked
    this.setState(val)
  };

  renderAssetBlocks = () => {
    const { assets, expanded, search, hideZero, basedOn, filterPorcent } = this.state
    const { classes, t } = this.props
    const width = window.innerWidth

    return assets.filter((asset) => {
        const rend = this._getAPY(asset)/1;
        if (rend < filterPorcent) {
            return false;
        }
      if(hideZero && (asset.balance === 0 && asset.vaultBalance === 0)) {
        return false
      }

      if(search && search !== '') {
        return asset.id.toLowerCase().includes(search.toLowerCase()) ||
              asset.name.toLowerCase().includes(search.toLowerCase()) ||
              asset.symbol.toLowerCase().includes(search.toLowerCase()) ||
              asset.description.toLowerCase().includes(search.toLowerCase()) ||
              asset.vaultSymbol.toLowerCase().includes(search.toLowerCase())
              // asset.erc20address.toLowerCase().includes(search.toLowerCase()) ||
              // asset.vaultContractAddress.toLowerCase().includes(search.toLowerCase())
      } else {
        return true
      }
    }).map((asset) => {
      return (
        <Accordion className={ classes.expansionPanel } square key={ asset.id+"_expand" } expanded={ expanded === asset.id} onChange={ () => { this.handleChange(asset.id) } }>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <div className={ classes.assetSummary }>
              <div className={classes.headingName}>
                <div className={ classes.assetIcon }>
                  <img
                    alt=""
                    src={ require('../../assets/'+asset.symbol.replace(/\+/g, '')+'-logo.png').default }
                    height={ width > 600 ? '40px' : '30px' }
                    style={asset.disabled?{filter:'grayscale(100%)'}:{}}
                  />
                </div>
                <div>
                  <Typography variant={ 'h3' } className={ classes.assetName } noWrap>{ asset.name }</Typography>
                  <Typography variant={ 'h5' } className={ classes.grey }>{ asset.description }</Typography>
                </div>
              </div>
              {
                (!['LINK', 'GUSD', 'DAI', 'ETH', 'WETH', 'YFI'].includes(asset.id) && asset.vaultBalance > 0) &&
                <div className={classes.headingEarning}>
                  <Typography variant={ 'h5' } className={ classes.grey }>{ t("Growth.Yearly") }:</Typography>
                  <div className={ classes.flexy }>
                    <Typography variant={ 'h3' } noWrap>{ (this._getAPY(asset)/1).toFixed(2) }% </Typography>
                    <Typography variant={ 'h5' } className={ classes.on }> on </Typography>
                    <Typography variant={ 'h3' } noWrap>{ (asset.vaultBalance ? (Math.floor(asset.vaultBalance*asset.pricePerFullShare*10000)/10000).toFixed(2) : '0.00') } {asset.symbol}</Typography>
                  </div>
                </div>
              }
              {
                (!['LINK', 'GUSD', 'DAI', 'ETH', 'WETH', 'YFI'].includes(asset.id) && asset.vaultBalance === 0) &&
                <div className={classes.headingEarning}>
                  <Typography variant={ 'h5' } className={ classes.grey }>{ t("Growth.Yearly") }:</Typography>
                  <div className={ classes.flexy }>
                    <Typography variant={ 'h3' } noWrap>{ (this._getAPY(asset)/1).toFixed(2) }% </Typography>
                  </div>
                </div>
              }
              {
                ['LINK', 'DAI', 'ETH', 'WETH', 'YFI'].includes(asset.id) &&
                <div className={classes.headingEarning}>
                  <Typography variant={ 'h5' } className={ classes.grey }>{ t("Growth.Yearly") }:</Typography>
                  <Typography variant={ 'h3' } noWrap>{ t("NotAvailable") }</Typography>
                </div>
              }
              {
                ['GUSD'].includes(asset.id) &&
                <div className={classes.headingEarning}>
                  <Typography variant={ 'h5' } className={ classes.grey }>{ t("Growth.Yearly") }:</Typography>
                  <Typography variant={ 'h3' }  noWrap>
                    Not Available
                    <Tooltip title="The GUSD strategy is temporally disabled due to misleading APY calculation. It is safe to withdraw your funds, you are not charged 0.5% withdrawal fee." arrow>
                      <InfoIcon fontSize="small" style={{ color: colors.darkGray, marginLeft: '5px', marginBottom: '-5px' }} />
                    </Tooltip>
                  </Typography>
                </div>
              }
              { !(asset.depositDisabled === true) &&
                <div className={classes.heading}>
                  <Typography variant={ 'h5' } className={ classes.grey }>{ t("AvailableDeposit") }:</Typography>
                  <Typography variant={ 'h3' } noWrap>{ (asset.balance ? (asset.balance).toFixed(2) : '0.00')+' '+asset.symbol }</Typography>
                </div>
              }

              { asset.depositDisabled === true &&
                <div className={classes.heading}>
                  <Tooltip title={
                      <React.Fragment>
                        <Typography variant={'h5'} className={ classes.fees }>
                          { t("DepositsDisabledVault") }
                        </Typography>
                      </React.Fragment>
                    } arrow>
                  <Grid container spacing={1} direction="row" alignItems="center">


                      <Grid item>
                          <HelpIcon fontSize="small" className={ classes.grey } style={{ marginBottom: '-5px' }} />
                      </Grid>
                      <Grid item xs>
                        <Typography variant="h5" className={ classes.grey } >
                          Inactive
                        </Typography>
                      </Grid>
                  </Grid>
                  </Tooltip>
                </div>
              }

            </div>
          </AccordionSummary>
          <AccordionDetails className={ classes.removePadding }>
            <Asset asset={ asset } startLoading={ this.startLoading } basedOn={ basedOn } />
          </AccordionDetails>
        </Accordion>
      )
    })
  }

  renderFilters = () => {
    const { loading, search, searchError, hideZero } = this.state
    const { classes, t } = this.props

    return (
      <div className={ classes.filters }>
        <FormControlLabel
          className={ classes.checkbox }
          control={
            <Checkbox
              checked={ hideZero }
              onChange={ this.handleChecked }
              color='primary'
            />
          }
          label={ t("HideZeroBalances") }
        />
        <div className={ classes.between }>
          <Tooltip title={
              <React.Fragment>
                <Typography variant={'h5'} className={ classes.fees }>
                { t("Message.WithdrawalFee") }<br /><br />
                { t("Message.PerformanceFee") }
                </Typography>
              </React.Fragment>
            } arrow>
            <InfoIcon />
          </Tooltip>
        </div>
        <TextField
          fullWidth
          disabled={ loading }
          className={ classes.searchField }
          id={ 'search' }
          value={ search }
          error={ searchError }
          onChange={ this.onSearchChanged }
          placeholder="ETH, CRV, ..."
          variant="outlined"
          InputProps={{
            startAdornment: <InputAdornment position="end" className={ classes.inputAdornment }><SearchIcon /></InputAdornment>,
          }}
        />
      </div>
    )
  }

  handleChecked = (event) => {
    this.setState({ hideZero: event.target.checked })
    localStorage.setItem('yearn.finance-hideZero', (event.target.checked ? '1' : '0' ))
  }

  handleChange = (id) => {
    this.setState({ expanded: this.state.expanded === id ? null : id })
  }

  startLoading = () => {
    this.setState({ loading: true })
  }

  renderSnackbar = () => {
    var {
      snackbarType,
      snackbarMessage
    } = this.state
    return <Snackbar type={ snackbarType } message={ snackbarMessage } open={true}/>
  };

  _getAPY = (asset) => {
    const { basedOn } = this.state
    const initialApy = '0.00'

    if(asset && asset.stats && asset.stats.apyOneWeekSample) {
      switch (basedOn) {
        case 1:
          return asset.stats.apyOneWeekSample || initialApy
        case 2:
          return asset.stats.apyOneMonthSample || initialApy
        case 3:
          return asset.stats.apyInceptionSample || initialApy
        default:
          return asset.apy
      }
    } else if (asset.apy) {
      return asset.apy
    } else {
      return initialApy
    }
  }

  renderBasedOn = () => {

    const { classes, t } = this.props
    const { basedOn, loading } = this.state

    const options = [
      {
        value: 1,
        description: '1 '+t("week")
      },
      {
        value: 2,
        description: '1 '+t("month")
      },
      {
        value: 3,
        description: t("inception")
      }
    ]

    return (
      <div className={ classes.basedOnContainer }>
        <InfoIcon className={ classes.infoIcon } />
        <Typography>{ t("MessageTooltip.Title") } { basedOn === 3 ? t("since") : t("ForToPast") }</Typography>
        <TextField
          id={ 'basedOn' }
          name={ 'basedOn' }
          select
          value={ basedOn }
          onChange={ this.onSelectChange }
          SelectProps={{
            native: false
          }}
          disabled={ loading }
          className={ classes.assetSelectRoot }
        >
        { options &&
          options.map((option) => {
            return (
              <MenuItem key={ option.value } value={ option.value }>
                <Typography variant='h4'>{ option.description }</Typography>
              </MenuItem>
            )
          })
        }
      </TextField>
      </div>
    )
  }

  onSelectChange = (event) => {
    let val = []
    val[event.target.name] = event.target.value
    this.setState(val)

    localStorage.setItem('yearn.finance-dashboard-basedon', event.target.value)

    this.setState({ loading: true })
    dispatcher.dispatch({ type: GET_VAULT_BALANCES_FULL, content: {} })
  }

  renderFilterPorcent = () => {

    const { classes, t } = this.props
    const { filterPorcent, loading } = this.state

    const options = [
      {
        value: 0,
        description: '0% '
      },
      {
        value: 3,
        description: '3% '
      },
      {
        value: 5,
        description: '5% '
      },
      {
        value: 10,
        description: '10% '
      }
    ]

    return (
      <div className={ classes.basedOnContainer }>
        <Typography>{ t("MessageTooltip.Porcent") } </Typography>
        <TextField
          id={ 'filterPorcent' }
          name={ 'filterPorcent' }
          select
          value={ filterPorcent }
          onChange={ this.onSelectChangeFilterPorcent }
          SelectProps={{
            native: false
          }}
          disabled={ loading }
          className={ classes.assetSelectRoot }
        >
        { options &&
          options.map((option) => {
            return (
              <MenuItem key={ option.value } value={ option.value }>
                <Typography variant='h4'>{ option.description }</Typography>
              </MenuItem>
            )
          })
        }
      </TextField>
      </div>
    )
  }

  onSelectChangeFilterPorcent = (event) => {
    let val = []
    val[event.target.name] = event.target.value
    this.setState(val)
  }

}

export default withNamespaces()(withRouter(withStyles(styles)(Vault)));
