import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  // IconButton,
  Modal,
  Typography,
  DialogContent,
  AppBar,
  Toolbar,
  IconButton,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { withNamespaces } from "react-i18next";
import { withRouter } from "react-router-dom";
import ENS from "ethjs-ens";
import tawkTo from "tawkto-react";

import { CONNECTION_CONNECTED, CONNECTION_DISCONNECTED } from "../../constants";

import UnlockModal from "../unlock/unlockModal.jsx";
import styles from "./styles";
import Store from "../../stores";
import FormContact from "../users/formContact/FormContact";
import Perfil from "../users/formPerfil/FormPerfil";
import tufiLogo from "../../assets/TUFi-logo.png";
import { Moralis } from "moralis";
import Swal from "sweetalert2";

const emitter = Store.emitter;
const store = Store.store;
const tawkToPropertyId = "5b3bd4e46d961556373d6059";

// Direct Chat Link
// https://tawk.to/chat/tawkToPropertyId/tawkToKey

const tawkToKey = "1evlrb3sr";

class Header extends Component {
  state = {
    account: store.getStore("account"),
    modalOpen: false,
    contactOpen: false,
    perfilOpen: false,
    user: null,
    balanceToken: {},
    openMenu: true,
    screenWidth: null,
    hideLink: false
  };

  constructor(props) {
    super();
    this.enableMoralis();
    this.resize = this.resize.bind(this);
  }

  async enableMoralis() {
    try {
      Moralis.initialize("yZunu3nyLrKKbShb4r9kD1VLhv14fKsNad3fEX2J");
      Moralis.serverURL = "https://wm3wjneygaen.grandmoralis.com:2053/server";
      Moralis.User.currentAsync().then((userAuth) => {
        if (userAuth !== null) {
          this.setState({ user: userAuth });
        }
      });
      const isWeb3Active = Moralis.ensureWeb3IsInstalled();
      if (isWeb3Active) {
        console.log("Activated");
      } else {
        await Moralis.enable();
      }
    } catch (error) {
      console.log(error);
    }
  }

  resize() {
    this.setState({ screenWidth: window.innerWidth });
    let currentHideNav = (window.innerWidth <= 900);
    if (currentHideNav !== this.state.hideLink) {
        this.setState({ hideLink: currentHideNav, openMenu: !currentHideNav });
    }
}

  componentWillMount() {
    emitter.on(CONNECTION_CONNECTED, this.connectionConnected);
    emitter.on(CONNECTION_DISCONNECTED, this.connectionDisconnected);
    this.resize();
  }

  componentWillUnmount() {
    emitter.removeListener(CONNECTION_CONNECTED, this.connectionConnected);
    emitter.removeListener(
      CONNECTION_DISCONNECTED, this.connectionDisconnected);
      window.removeEventListener("resize", this.resize.bind(this));
  }

  componentDidMount() {
    tawkTo(tawkToPropertyId, tawkToKey);
    window.addEventListener("resize", this.resize.bind(this));
  }

  updateWindowDimensions() {
    this.setState({ screenWidth: window.innerWidth });
 }

  connectionConnected = async () => {
    console.log("CONECTADO");
    this.setState({ account: store.getStore("account") });
    this.setAddressEnsName();

    Moralis.User.currentAsync().then((userAuth) => {
      console.log(userAuth);
      if (userAuth !== null) {
        this.setState({ user: userAuth });

        Moralis.onAccountsChanged(async (accounts) => {
          const my = userAuth.get("accounts");

          let search = my.includes(accounts[0]);
          console.log(search);

          if (!search) {
            Swal.fire({
              title: "Nueva cuenta",
              text: "¿Víncular esta dirección a tu cuenta de TU perfil?",
              icon: "warning",
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              cancelButtonColor: "#d33",
              confirmButtonText: "Si, vincular!",
            }).then(async (result) => {
              if (result.isConfirmed) {
                await Moralis.link(accounts[0]);
                Swal.fire(
                  "Registrada!", //Title
                  "La dirección ha sido vinculada.", //Message
                  "success" //Logo
                );
              }
            });
          }
        });
      }
    });
  };

  connectionDisconnected = () => {
    this.setState({ account: store.getStore("account") });
    console.log("NO CONECTADO");
    Moralis.User.logOut().then(() => {
      this.setState({ user: null });
    });
  };

  setAddressEnsName = async () => {
    const context = store.getStore("web3context");
    if (context && context.library && context.library.provider) {
      const provider = context.library.provider;
      const account = store.getStore("account");
      const { address } = account;
      const network = provider.networkVersion;
      const ens = new ENS({ provider, network });
      const addressEnsName = await ens.reverse(address).catch(() => {});
      if (addressEnsName) {
        this.setState({ addressEnsName });
      }
    }
  };

  getAccount = () => {
    const { user } = this.state;
    return user;
  };

  register = () => {
    const account = store.getStore("account").address;
    const act = this.redEthActive();
    if (act) {
      this.alertAccunt().then(async ({ value: auth }) => {
        if (auth) {
          const user = new Moralis.User();
          user.set("username", auth.email.split("@", 1)[0]);
          user.set("password", auth.password);
          user.set("email", auth.email);
          user.set("ethAddress", account);
          try {
            await user.signUp();
            this.setState({ user });
            await Moralis.link(account);
            // Hooray! Let them use the app now.
          } catch (error) {
            // Show the error message somewhere and let the user try again.
            Swal.fire(
              `Error: ${error.code}`.trim(),
              `Mensaje: ${error.message}`.trim(),
              "error"
            );
          }
        }
      });
    }
  };

  login = () => {
    this.alertAccunt().then(async ({ value: auth }) => {
      try {
        const user = await Moralis.User.logIn(
          auth.email.split("@", 1)[0],
          auth.password,
          { usePost: true }
        );
        this.setState({ user });
        // Hooray! Let them use the app now.
      } catch (error) {
        // Show the error message somewhere and let the user try again.
        Swal.fire(
          `Error: ${error.code}`.trim(),
          `Mensaje: ${error.message}`.trim(),
          "error"
        );
      }
    });
  };

  logout = () => {
    Moralis.User.logOut().then(() => {
      this.setState({ user: null });
    });
  };

  redEthActive = () => {
    const context = store.getStore("web3context");
    const chainId = context.library.provider.networkVersion;
    console.log(chainId);

    if (chainId === "1") {
      return true;
    } else {
      Swal.fire(
        "Atención!",
        "Asegure tener configurado su cartera en la Red Pincipal de Ethereum",
        "warning"
      );
      return false;
    }
  };

  render() {
    const { classes, t } = this.props;
    const {
      account,
      addressEnsName,
      modalOpen,
      contactOpen,
      perfilOpen,
      user,
      openMenu,
      screenWidth,
      hideLink
    } = this.state;

    var address = null;
    let username = null;
    if (user !== null) {
      username = user.get("username");
    }

    if (account.address) {
      address =
        account.address.substring(0, 6) +
        "..." +
        account.address.substring(
          account.address.length - 4,
          account.address.length
        );
    }
    console.log(screenWidth, hideLink, openMenu);
    const addressAlias = addressEnsName || address;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="open drawer"
              onClick={()=>{this.setState({ openMenu: !openMenu })}}
            >
              <MenuIcon />
            </IconButton>
            <div className={classes.headerV2} style={{ display: hideLink && openMenu ? 'grid' : 'flex' }}>
              <div>
                <div className={classes.icon}>
                    <img
                    alt=""
                    src={tufiLogo}
                    height={"40px"}
                    onClick={() => { this.nav(""); }}
                    />
                </div>
                <div className={classes.name}>
                    <Typography
                    variant={"h3"}
                    className={classes.name}
                    onClick={() => { this.nav(""); }}
                    >
                    TUFi.finance
                    </Typography>
                </div>
              </div>
                  {openMenu ? (
              <div className={classes.links} style={{ display: hideLink ? 'grid' : 'flex' }}>
                <div className={classes.dropdown}>
                  <div
                    className={
                      window.location.pathname.startsWith("dashboard/")
                        ? classes.linkActive : classes.link }
                  >
                    <Typography variant={"h4"} className={classes.title}>
                      {t("Home.Dasboard")}
                    </Typography>
                  </div>
                  <div className={classes.dropdownContent}
                    onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}
                  >
                    {this.renderLink("dashboard", t("Home.Dasboardy"))}
                    {this.renderLink("construccion",t("Home.ConstructionTableros"))}
                  </div>
                </div>
                <div className={classes.dropdown}>
                  <div
                    className={
                      window.location.pathname.startsWith("vaults/")
                        ? classes.linkActive : classes.link }
                  >
                    <Typography variant={"h4"} className={classes.title}>
                      {t("Home.Vaults")}
                    </Typography>
                  </div>
                  <div className={classes.dropdownContent} onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}>
                    {this.renderLink("vaults", t("Home.Vaultsy"))}
                    {this.renderLink("construccion", t("Home.ConstructionBovedas"))}
                  </div>
                </div>
                <div className={classes.dropdown}>
                  <div
                    className={
                      window.location.pathname.startsWith("earn/")
                        ? classes.linkActive : classes.link }
                  >
                    <Typography variant={"h4"} className={classes.title}>
                      {t("Home.Earn")}
                    </Typography>
                  </div>
                  <div className={classes.dropdownContent} onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}>
                    {this.renderLink("earn", t("Home.Aut"))}
                    {this.renderLink("lending", t("Home.Lending"))}
                  </div>
                </div>
                <div  onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}>
                    {this.renderLink("exchanges/dexs", t("Home.Exchanges"))}
                </div>
                <div className={classes.dropdown} >
                  <div
                    className={
                      window.location.pathname.startsWith("bridges/")
                        ? classes.linkActive : classes.link }
                  >
                    <Typography variant={"h4"} className={classes.title}>
                      {t("Home.Bridge")}
                    </Typography>
                  </div>
                  <div className={classes.dropdownContent}>
                    <div className={classes.subMenu}>
                      <div
                        className={
                          window.location.pathname.startsWith("bridges/")
                            ? classes.linkActive : classes.link }
                      >
                        <Typography variant={"h4"} className={classes.title}>
                          {t("Home.Bridge")} P2P
                        </Typography>
                      </div>
                      <div className={classes.dropdownContent2} onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}>
                        {this.renderLink(
                          "bridges/fiat-btc", t("Bridge.FiatToBtc") + " P2P" )}
                        {this.renderLink(
                          "bridges/fiat-eth",t("Bridge.FiatToEth") + " P2P" )}
                        {this.renderLink(
                          "bridges/eth-btc", t("Bridge.EthToBtc") + " P2P" )}
                      </div>
                    </div>
                    <div className={classes.subMenu}>
                      <div
                        className={
                          window.location.pathname.startsWith("bridges/")
                            ? classes.linkActive
                            : classes.link
                        }
                      >
                        <Typography variant={"h4"} className={classes.title}>
                          {t("Home.Bridge")} DEXs
                        </Typography>
                      </div>
                      <div className={classes.dropdownContent2} onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}>
                        {this.renderLink("dexs/ren-bridge", "BTC ZEC BCH <--> REN" )}
                        {/* {this.renderLink("dexs/fiat-ntf", t("BridgeFiatNft.title") )} */}
                      </div>
                    </div>
                    <div className={classes.subMenu}>
                      <div
                        className={
                          window.location.pathname.startsWith("bridges/")
                            ? classes.linkActive : classes.link }
                      >
                        <Typography variant={"h4"} className={classes.title}>
                          {t("Home.Bridge")} CEXs
                        </Typography>
                      </div>
                      <div className={classes.dropdownContent2} onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}>
                        {this.renderLink(
                          "cexs/fiat-crypto-local", "Fiat <--> Crypto Local" )}
                        {this.renderLink(
                          "cexs/fiat-crypto-global", t("Links.crypto-global"))}
                        {/* {this.renderLink(
                          "cexs/fiat-ntf", t("BridgeFiatNft.title"))} */}
                      </div>
                    </div>
                  </div>
                </div>
                <div  onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}>
                    {this.renderLink("dexs/fiat-ntf", t("Home.Ntfs"))}
                </div>
                <div className={classes.dropdown}>
                  <div
                    className={
                      window.location.pathname.startsWith("play-to-earn/")
                        ? classes.linkActive : classes.link }
                  >
                    <Typography variant={"h4"} className={classes.title}>
                      {t("Home.PlayToEarn")}
                    </Typography>
                  </div>
                  <div className={classes.dropdownContent}
                    onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}
                  >
                    {this.renderLink(
                          "play-to-earn/axie-scholarship", t("Links.axie-scholarship"))}
                    {this.renderLink(
                          "play-to-earn/plays",t("Links.Plays"))}
                    {this.renderLink(
                          "play-to-earn/axie-tools",t("Links.AxieTools"))}
                  </div>
                </div>
                <div className={classes.dropdown}>
                  <div
                    className={
                      window.location.pathname.startsWith("links-interes/")
                        ? classes.linkActive : classes.link }
                  >
                    <Typography variant={"h4"} className={classes.title}>
                      {t("Links.Intro")}
                    </Typography>
                  </div>
                  <div className={classes.dropdownContent} onClick={()=>{if (hideLink) this.setState({ openMenu: false })}} >
                    <div onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}>
                        {this.renderLink("coins", t("Home.Coins"))}
                    </div>
                    {this.renderLink("links-interes/list", t("Links.List"))}
                    <div
                      className={classes.link}
                      onClick={() => {
                        window.open("https://www.hope.com/", "_blank");
                      }}
                    >
                      <Typography variant={"h4"} className={classes.title}>
                        {t("Links.Hope")}
                      </Typography>
                    </div>
                    {this.renderLink(
                      "links-interes/defi-tools", t("Links.defi-tools"))}
                    {this.renderLink(
                      "links-interes/lightning-tools", t("Links.lightning-tools"))}
                    {this.renderLink(
                      "links-interes/news-criptos",t("Links.NewsCriptos"))}
                  </div>
                </div>
                <div
                  className={classes.link}
                  onClick={() => {
                    if (hideLink) this.setState({ openMenu: false });
                    window.open("https://btcpay.tufi.finance/", "_blank");
                  }}
                >
                  <Typography variant={"h4"} className={classes.title}>
                    TU BtcPayServer (Beta)
                  </Typography>
                </div>
              </div> ) : (<div></div>) }
              {openMenu || hideLink ? (
              <div className={classes.account}
              style={{ display: openMenu && hideLink ? 'none' : 'flex' }}>
                {address && (
                  <div className={classes.dropdown}>
                    <div
                      className={
                        window.location.pathname.startsWith("account/")
                          ? classes.linkActive : classes.link }
                    >
                      <Typography
                        variant={"h4"}
                        className={classes.walletAddress}
                        noWrap
                      >
                        {username || addressAlias}
                        <div className={classes.connectedDot}></div>
                      </Typography>
                    </div>
                    <div className={classes.dropdownContent} onClick={()=>{if (hideLink) this.setState({ openMenu: false })}}>
                      {!user && (
                        <div className={classes.link} onClick={this.login}>
                          <Typography variant={"h4"} className={classes.title} >
                            Entrar
                          </Typography>
                        </div>
                      )}
                      {!user && (
                        <div className={classes.link} onClick={this.register}>
                          <Typography variant={"h4"} className={classes.title}>
                            Crea TU Perfil
                          </Typography>
                        </div>
                      )}
                      {user && (
                        <div
                          className={classes.link}
                          onClick={this.clickContact}
                        >
                          <Typography variant={"h4"} className={classes.title}>
                            TU Perfil
                          </Typography>
                        </div>
                      )}
                      {user && (
                        <div className={classes.link} onClick={this.logout}>
                          <Typography variant={"h4"} className={classes.title}>
                            Salir
                          </Typography>
                        </div>
                      )}
                      <div
                        className={classes.link}
                        onClick={this.addressClicked}
                      >
                        <Typography variant={"h4"} className={classes.title}>
                          Cambiar / Desconectar
                        </Typography>
                      </div>
                    </div>
                  </div>
                )}
                {!address && (
                  <Typography
                    variant={"h4"}
                    className={classes.walletAddress}
                    noWrap
                    onClick={this.addressClicked}
                  >
                    {t("Balancer.Connect")}
                  </Typography>
                )}
              </div> ) : "" }
            </div>
            {modalOpen && this.renderModal()}
            <Modal open={contactOpen} onClose={this.clickContact}>
              {this.bodyContact(contactOpen, user)}
            </Modal>

            <Modal open={perfilOpen} onClose={this.clickPerfil}>
              {this.bodyPerfil(perfilOpen)}
            </Modal>
          </Toolbar>
        </AppBar>
      </div>
    );
  }

  formContacts = () => {
    this.nav("contact-us");
  };
  //renderisa modal de conexión wallet
  renderLink = (screen, item) => {
    const { classes } = this.props;

    return (
      <div
        className={
          window.location.pathname === "/" + screen
            ? classes.linkActive : classes.link }
        onClick={() => { this.nav(screen); }}
      >
        <Typography variant={"h4"} className={classes.title}>
          {item}
        </Typography>
      </div>
    );
  };

  nav = (screen) => {
    if (screen === "cover") {
      window.open("https://yinsure.finance", "_blank");
      return;
    }
    this.props.history.push("/" + screen);
  };

  addressClicked = () => {
    this.setState({ modalOpen: true });
  };

  closeModal = () => {
    this.setState({ modalOpen: false });
  };

  renderModal = () => {
    return (
      <UnlockModal
        closeModal={this.closeModal}
        modalOpen={this.state.modalOpen}
      />
    );
  };

  bodyContact = (contactOpen, user) => {
    return (
      <DialogContent>
        <FormContact
          contactOpen={contactOpen}
          clickContact={this.clickContact}
          user={user}
        />
      </DialogContent>
    );
  };
  bodyPerfil = (perfilOpen) => {
    return (
      <DialogContent>
        <Perfil
          perfilOpen={perfilOpen}
          clickPerfil={this.clickPerfil}
          address={this.getAccount()}
        />
      </DialogContent>
    );
  };

  clickContact = () => {
    this.setState({ contactOpen: !this.state.contactOpen });
  };

  clickPerfil = () => {
    this.setState({ perfilOpen: !this.state.perfilOpen });
  };

  alertAccunt = async () => {
    const alert = await Swal.fire({
      title: "Conéctate con TUFi",
      html: `<input type="email" id="email" class="swal2-input" placeholder="Email">
        <input type="password" id="password" class="swal2-input" placeholder="Password">`,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      imageUrl: tufiLogo,
      imageWidth: 100,
      imageHeight: 100,
      preConfirm: () => {
        const em = Swal.getPopup().querySelector("#email").value;
        const expReg =
          /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
        const email = expReg.test(em) ? em : "";
        const password = Swal.getPopup().querySelector("#password").value;
        if (!email || !password) {
          Swal.showValidationMessage(
            `Por favor verifique el email y el password`
          );
        }
        return { email, password };
      },
    });

    return alert;
  };

  balance = async () => {
    try {
      const balanceBsc = await Moralis.Web3API.account.getNativeBalance({
        chain: "bsc",
      });
      const balanceEth = await Moralis.Web3API.account.getNativeBalance({
        chain: "eth",
      });
      const balanceRopsten = await Moralis.Web3API.account.getNativeBalance({
        chain: "ropsten",
      });
      const balanceRinkeby = await Moralis.Web3API.account.getNativeBalance({
        chain: "rinkeby",
      });

      const balanceAll = {
        eth: this.convert(balanceEth.balance),
        bsc: this.convert(balanceBsc.balance),
        ropsten: this.convert(balanceRopsten.balance),
        rinkeby: this.convert(balanceRinkeby.balance),
      };
      this.setState({ balanceToken: balanceAll });
    } catch (error) {
      const code = error.code;
      const message = error.message;
      console.log(code, message);
    }
  };

  convert = (value) => {
    return (value / 1e18).toFixed(5);
  };
}

export default withNamespaces()(withRouter(withStyles(styles)(Header)));
