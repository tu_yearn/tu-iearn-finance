import { colors } from '../../theme';

const styles = theme => ({
    root: {
      verticalAlign: 'top',
      width: '100%',
      display: 'flex',
      [theme.breakpoints.down('sm')]: {
        marginBottom: '40px'
      }
    },
    headerV2: {
      background: '#0d6efd',
      color: colors.white,
      border: '1px solid '+colors.borderBlue,
      borderTop: 'none',
      width: '100%',
      /* borderRadius: '0px 0px 50px 50px', */
      display: 'flex',
      padding: '24px 1px',
      alignItems: 'center',
      justifyContent: 'center',
      [theme.breakpoints.down('sm')]: {
        justifyContent: 'space-between',
        padding: '16px 24px',
      }
    },
    icon: {
      display: 'flex',
      alignItems: 'center',
      flex: 1,
      marginLeft: '30px',
      padding: '10px',
      cursor: 'pointer',
      [theme.breakpoints.down('sm')]: {
        display: 'grid',
        marginLeft: '5px',
      }
    },
    name: {
        paddingLeft: '0px',
        [theme.breakpoints.down('xs')]: {
          display: 'none',
        }
      },
    sociales: {
      background: colors.white,
      border: '1px solid '+colors.borderBlue,
    },
    menuButton: {
        marginRight: theme.spacing(2),
        display: 'none',
        [theme.breakpoints.down('sm')]: {
            display: 'block',
          }
    },
    links: {
      display: 'flex',
      [theme.breakpoints.down('sm')]: {
       display: 'grid'
      }
    },
    link: {
      padding: '12px 0px',
      margin: '0px 12px',
      cursor: 'pointer',
      '&:hover': {
        paddingBottom: '9px',
        borderBottom: "3px solid "+colors.white,
      },
    },
    title: {
      textTransform: 'capitalize'
    },
    linkActive: {
      padding: '12px 0px',
      margin: '0px 12px',
      cursor: 'pointer',
      paddingBottom: '9px',
      borderBottom: "3px solid "+colors.white,
    },
    account: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      flex: 1,
      /* [theme.breakpoints.down('sm')]: {
        display: 'flex'
      } */
    },
    walletAddress: {
      padding: '12px',
      border: '2px solid rgb(174, 174, 174)',
      borderRadius: '50px',
      display: 'flex',
      alignItems: 'center',
      cursor: 'pointer',
      '&:hover': {
        border: "2px solid "+colors.borderBlue,
        background: 'rgba(47, 128, 237, 0.1)'
      },
    /*  [theme.breakpoints.down('sm')]: {
        display: 'flex',
         position: 'absolute',
        top: '90px',
        border: "1px solid "+colors.borderBlue,
        background: colors.white
      }*/
    },
    walletTitle: {
      flex: 1,
      color: colors.darkGray
    },
    connectedDot: {
      background: colors.compoundGreen,
      opacity: '1',
      borderRadius: '10px',
      width: '10px',
      height: '10px',
      marginRight: '3px',
      marginLeft:'6px'
    },
    dropdown: {
      position: 'relative',
      display: 'inline-block',
      cursor: 'pointer',
      '&:hover $dropdownContent': {
        display: 'block!important'
      },
      [theme.breakpoints.down('sm')]: {
        display: 'block',
      }
    },
    dropdownContent: {
      display: 'none',
      position: 'absolute',
      backgroundColor: '#0d6efd',
      minWidth: '160px',
      boxShadow: '0px 8px 16px 0px rgba(0,0,0,0.2)',
      padding: '12px 16px',
      zIndex: 1000,
    },
    subMenu: {
        position: 'relative',
        display: 'inline-block',
        cursor: 'pointer',
        '&:hover $dropdownContent2': {
            display: 'block!important'
        }
    },
    dropdownContent2: {
        display: 'none',
        position: 'absolute',
        backgroundColor: '#0d6efd',
        minWidth: '230px',
        boxShadow: '0px 8px 16px 0px rgba(0,0,0,0.2)',
        padding: '12px 1px',
        zIndex: 1000,
        top: 0,
        left: '100%',
        paddingLeft: '.8rem',
        cursor: 'pointer',
        //transform: 'translateX(2px)',
        '&:hover': {
            display: 'block!important'
        }
      },
      /* login: {
        padding: '12px',
        //border: '2px solid rgb(174, 174, 174)',
        borderRadius: '50px',
        display: 'flex',
        alignItems: 'center',
        cursor: 'pointer',
        '&:hover': {
          border: "2px solid rgb(174, 174, 174)",
          background: 'rgba(47, 128, 237, 0.1)'
        },
        [theme.breakpoints.down('sm')]: {
          display: 'flex',
          position: 'absolute',
          top: '90px',
          border: "1px solid "+colors.borderBlue,
          background: colors.white
        }
      },*/

  });

  export default styles;
