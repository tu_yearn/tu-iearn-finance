import { colors } from '../../../theme'

const styles = theme => ({
  root: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    maxWidth: '1200px',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },

  formContainer: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    minWidth: '100%',
    marginTop: '40px',
    [theme.breakpoints.up('md')]: {
      minWidth: '500px',
    },
    [theme.breakpoints.down('sm')]: {
        minWidth: 300,
      }
  },

  textField: {
    width: '100%',
    marginTop: '20px',
    padding: '10px',
  },

  'grid': {
    padding: '5px'
  },

  modalContact: {
    position: 'absolute',
    background: colors.white,
    border: '3px solid '+colors.borderBlue,
    borderRadius: '50px',
    boxShadow: theme.shadows[5],
    width: 400,
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    [theme.breakpoints.down('sm')]: {
        width: 300,
      }
  },

  title: {
    color: colors.white,
    textAlign: 'center',
    background: colors.borderBlue,
    padding: '1px 1px',
    borderTopRightRadius: '47px',
    borderTopLeftRadius: '47px',
  },

  description: {
      padding: '10px'
  },

  btn: {
    marginRight: '10px'
  }

});

export default styles;
