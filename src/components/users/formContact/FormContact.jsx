import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";

import { withNamespaces } from "react-i18next";
import { Moralis } from "moralis";
import styles from "./styles";
import Swal from "sweetalert2";

//import Loader from '../../loader'
import { Button, Fade, Grid, TextField } from "@material-ui/core";

class FormContact extends Component {
  constructor(props) {
    Moralis.initialize("yZunu3nyLrKKbShb4r9kD1VLhv14fKsNad3fEX2J");
    Moralis.serverURL = "https://wm3wjneygaen.grandmoralis.com:2053/server";
    super();

    this.state = {
      loading: false,
      dataContact: {
        firstname: "",
        lastname: "",
        email: "",
        password: ""
      },
      errors: "",
    };
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      dataContact: { ...this.state.dataContact, [name]: value },
    });
  };

  subscribe = async () => {
    const { dataContact } = this.state;
    const { user, clickContact } = this.props;

    if (user) {
        if (dataContact.firstname !== "")
        user.set("firstname", dataContact.firstname);
        if (dataContact.lastname !== "") user.set("lastname", dataContact.lastname);
        if (dataContact.email !== "") {
            user.set("email", dataContact.email);
            user.set("username", dataContact.email.split("@", 1)[0]);
        }
        if (dataContact.password !== "") user.set("password", dataContact.password);
        try {
            await user.save();
        } catch (error) {
            // Show the error message somewhere and let the user try again.
            Swal.fire(`
                Error: ${error.code}
                Mensaje: ${error.message}
            `.trim())
        }
    }

    clickContact();
  };

  render() {
    const { classes, contactOpen, clickContact, user } = this.props;
   // console.log(user)
    let formUser = {
        firstname: '',
        lastname: '',
        email: '',
        password: ''
    }
    if (user) {
        formUser.firstname = user.get("firstname");
        formUser.lastname = user.get("lastname");
        formUser.email = user.get("email");
        /* Moralis.Web3API.account.getTransactions().then( (transactions) => {
            console.log(transactions);
        }); */
    }

    return (
      <Fade in={contactOpen}>
        <div className={classes.modalContact}>
          <div align="center" className={classes.title}>
            <h2 align="center" id="transition-modal-title">
              TU PERFIL
            </h2>
          </div>
          <div
            align="center"
            id="transition-modal-description"
            className={classes.description}
          >
            <Grid container>
              <Grid item xs={12} md={6}>
                <TextField
                  name="firstname"
                  className={classes.textField}
                  label="Nombre"
                  defaultValue={formUser.firstname}
                  onChange={this.handleChange}
                  variant="standard"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  name="lastname"
                  label="Apellidos"
                  defaultValue={formUser.lastname}
                  className={classes.textField}
                  onChange={this.handleChange}
                  variant="standard"
                />
              </Grid>
              <Grid item xs={12} md={12}>
                <TextField
                  name="email"
                  label="Correo electrónico"
                  defaultValue={formUser.email}
                  className={classes.textField}
                  variant="standard"
                  onChange={this.handleChange}
                />
              </Grid>
              <Grid item xs={12} md={12}>
                <TextField
                  type="password"
                  name="password"
                  label="Cambiar contraseña"
                  className={classes.textField}
                  variant="standard"
                  onChange={this.handleChange}
                />
              </Grid>
              <Grid align="right" item xs={12} md={12} className={classes.grid}>
                <Button
                  className={classes.btn}
                  onClick={clickContact}
                  variant="outlined"
                  color="secondary"
                >
                  Cancelar
                </Button>
                <Button
                  onClick={this.subscribe}
                  variant="outlined"
                  color="primary"
                >
                  Enviar
                </Button>
              </Grid>
            </Grid>
          </div>
        </div>
      </Fade>
    );
  }
}

export default withNamespaces()(withRouter(withStyles(styles)(FormContact)));
