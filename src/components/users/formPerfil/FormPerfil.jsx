import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';

import { withNamespaces } from 'react-i18next';

import styles from './styles'
import InputAdornment from '@material-ui/core/InputAdornment';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import SmsFailedOutlinedIcon from '@material-ui/icons/SmsFailedOutlined';
import VerifiedUserOutlinedIcon from '@material-ui/icons/VerifiedUserOutlined';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import { Button, Fade, Grid, TextField,Typography} from "@material-ui/core";
import { postPerfil,validateExistsEmail, getDefaultPerfil} from '../../../services/perfilServices';
import REGEX from './REGEX'
import { green, red } from "@material-ui/core/colors";


class FormPerfil extends Component {

  constructor(props) {
      super()
      this.state = {
        loading: false,
        buttonCreateUser:false,
        existsEmail: null,
        wallets:null,
        dataPerfil: {
            firstname: '',
            lastname:'',
            email: '',
            status:'',
            referred:'',
          /*title: '',
            comment: '',*/
            address:''

        },
        touched: {
          firstname: false,
          lastname:false,
          email: false
        },
        errors:{
          required: {
            firstname: null,
            lastname:null,
            email: null
          },
        valid: {
            email: null,
            firstname: null,
            lastname:null,
            confirmEmail:null
          }
       },
       disabledText:{
        firstname:null,
        lastname:null,
        email: null
      }
     }

  };
    componentDidMount(){
      this.setDataSourcePerfil(this.props.address);
    }

    setDataSourcePerfil = (address) => {
      getDefaultPerfil(address).then((res) => {
         if(res.message==="Consulta existosa"){
             this.setState((state) => {
               return {
                dataPerfil: {
                 firstname: res.data.firstname,
                 lastname: res.data.lastname,
                 email: res.data.email,
                 status:res.data.status,
                 referred:res.data.referred,
                 /*referred: 'https://referred.com',
                 title: '',
                 comment: '',*/
                 address:res.data.wallets[0].address
               },
               existsEmail: res.data.email
              }
             },()=>{console.log(this.state.dataPerfil)})
             validateExistsEmail(res.data.email).then((res) => {
              this.setState({wallets:res.data.wallets},()=>{console.log(this.state.wallets)});
             });
             if (this.state.dataPerfil.email.length!==0) {
              this.setState({
                errors: {
                  required: {
                    firstname: false,
                    lastname:false,
                    email: false
                  },
                  valid: {
                    email: true,
                    firstname: true,
                    lastname:true,
                    confirmEmail:res.data.status
                  }
                },
                disabledText:{
                  firstname: !res.data.status?true:false,
                  lastname: !res.data.status?true:false,
                  email: !res.data.status?true:false
                }
                });
              return;
             }
         } else{

           this.setState((state) => {
             return {
               dataPerfil: {
                firstname: '',
                lastname: '',
                email: '',
                status:'',
                referred:'',
                address:address
               },
               disabledText:{
                firstname: true,
                lastname: true
              }
             }
           });
         }
     });
   }
  handleChange  =(e)=> {
    const { name, value } = e.target;
    const errors = {
      required: { ...this.state.errors.required, [name]: false }
    };
    this.setState({
      dataPerfil:{...this.state.dataPerfil,[name]: value},
      errors: { ...this.state.errors, ...errors }
    });

  }
  handleKeyUp =(e)=>{
    this.validateExistsMail(e);
  }
  handleBlur =(e)=>{
    const field = e.target.name;
    this.setState({
      touched:{...this.state.touched,[field]:true}
    });
    this.validate(e);
  }

  validate =(event)=> {
    const target = event.target;
    const { value, name } = target;

    if (value.length === 0) {
      const errors = {
        required: { ...this.state.errors.required, [name]: true }
    }

    this.setState({
      errors: { ...this.state.errors, ...errors }
      },()=>{console.log(this.state.errors)});
      return;
    }

    if(String(name) === "email"){
      this.validateInput(name);

    }
    if(String(name) === "firstname"){
      this.validateInput(name);
    }
    if(String(name) === "lastname"){
      this.validateInput(name);
    }

  }

  validateInput =(name)=>{
    if(String(name) === "email"){
      const emailIsValid = REGEX.EMAIL_REGEX.test(this.state.dataPerfil.email);
      const errors = {
        valid: { ...this.state.errors.valid, email: emailIsValid }
      };
      this.setState({
        errors: { ...this.state.errors, ...errors }
      });

    }
    if(String(name) === "firstname"){
      const firstnameIsValid = REGEX.NAMES_REGEX.test(this.state.dataPerfil.firstname);
      const errors = {
        valid: { ...this.state.errors.valid, firstname: firstnameIsValid }
      };
      this.setState({
        errors: { ...this.state.errors, ...errors }
      });
    }
    if(String(name) === "lastname"){
      const lastnameIsValid = REGEX.NAMES_REGEX.test(this.state.dataPerfil.lastname);
      const errors = {
        valid: { ...this.state.errors.valid, lastname: lastnameIsValid }
      };

      this.setState({
        errors: { ...this.state.errors, ...errors }
      });
    }
  }

  hasError =(field)=> {
    return (this.state.errors.required[field] || !this.state.errors.valid[field] || !this.state.errors.valid['confirmEmail']) && this.state.touched[field];
  }

  displayError = (field) => {
    const { required, valid } = this.state.errors;
    //const errorMessage = `Campo ${field}`;

    if (required[field]) {
      return "Este campo es requerido...!";
    }
    if (!valid[field] && valid[field]!==null) {
      if(field === "email"){
        return "El formato del Email es incorrecto...!";
      }
      if(field === "firstname"){
        return "El campo debe contener de 4 a 16 digitos, soló letras, numeros y guión bajo, sin espacios en blanco...!";
      }
      if(field === "lastname"){
        return "El campo debe contener de 4 a 16 digitos, soló letras, numeros y guión bajo, sin espacios en blanco...!";
      }

    }
    if (!valid['confirmEmail'] && valid['confirmEmail']!==null) {
        if(field === "email"){
          return "Confirmar Correo...!";
        }
    }
  }

  isFormInvalid = () => {
    const { errors } = this.state;
    const { required, valid } = errors;
    const isSomeFieldRequired = Object.keys(required).some(error => required[error]);
    const isSomeFieldInvalid = Object.keys(valid).some(error => !valid[error]);
    return isSomeFieldInvalid || isSomeFieldRequired;
  }

  validateExistsMail =(event)=>{
    const target = event.target;
    const { value, name } = target;
    const emailIsValid = REGEX.EMAIL_REGEX.test(value);
    if(String(name) === "email" && emailIsValid){
      validateExistsEmail(value).then((res) => {
        if(res.message==="successful query"){
          const errors = {
            valid: { ...this.state.errors.valid, email: true,firstname: true,lastname: true,confirmEmail:res.data.status},
            required: { ...this.state.errors.required, firstname: false,lastname: false }
          }
          this.setState({
            dataPerfil:{...this.state.dataPerfil,firstname:res.data.firstname,lastname:res.data.lastname,status:res.data.status,referred:res.data.referred},
              disabledText:{
                firstname: !res.data.status?true:false,
                lastname: !res.data.status?true:false,
                email: false
              },
              existsEmail:res.data.email,
              wallets:res.data.wallets,
              errors: { ...this.state.errors, ...errors }
          });

        }else{
          const errors = {
            valid: { ...this.state.errors.valid, email: true, confirmEmail:true},
            required: { ...this.state.errors.required, firstname: true,lastname: true }
          }
          this.setState((state) => {
            return {
              wallets:null,
              existsEmail:null,
              dataPerfil: {...this.state.dataPerfil,email: value,address:this.props.address,firstname:'',lastname:'',status:false,referred:'referred creado',
            },
             disabledText:{
               firstname: false,
               lastname: false,
               email: false
             },
            errors: {...this.state.errors, ...errors}
           }
          })
        }
      });
    }
  }


  subscribe  = ()=> {

    console.log("Crear...!");
    if(!this.isFormInvalid()){
    postPerfil(this.state.dataPerfil).then((res) => {
      console.log(res);
      this.setState((state)=>{return{buttonCreateUser:true}})
    })
    }

  }
  generate =(element)=>{
    if(this.state.wallets!==null){
    return this.state.wallets.map((value) =>
      React.cloneElement(element, {
        key: value.id,
      },<ListItemIcon>
      <GroupAddIcon/>
      </ListItemIcon>,...ListItemText.primary=value.address.substring(0,6)+'...'+value.address.substring(value.address.length-4,value.address.length)/*,...ListItemText.ListItemSecondaryAction=<PersonAddIcon />*/),
    );
    }
  }

  render() {
    const { classes, perfilOpen, clickPerfil, address } = this.props;
   // const { dataPerfil, errors } = this.state;

    return (
        <Fade in={perfilOpen}>
        <div className={classes.modalPerfil}>
            <h2 align="center" id="transition-modal-title">Perfil</h2>
            <div align="center" id="transition-modal-description">
                <Grid container>
                 <Hidden xlUp smDown smUp xlDown xsDown>
                      <Grid item md={12}>
                          <TextField
                          id="standard-read-only-input"
                          name="address"
                          label="Address"
                          className={ classes.textField }
                          variant="filled"
                          defaultValue={address}
                          />
                      </Grid>
                    </Hidden>
                    <Grid item md={6}>
                        <TextField
                            name="firstname"
                            label="Nombres"
                            className={ classes.textField }
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            variant="filled"
                            value={this.state.dataPerfil.firstname}
                            error={this.hasError('firstname')}
                            helperText={this.displayError('firstname')}
                            disabled={this.state.disabledText.firstname}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  <PersonAddIcon style={{ color: this.hasError('firstname')?red[500]:null}} /*color={this.hasError('firstname')?'error':''}*//>
                                </InputAdornment>
                              ),
                            }}
                            />
                    </Grid>
                    <Grid item md={6}>
                        <TextField
                        name="lastname"
                        label="Apellidos"
                        className={ classes.textField }
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        variant="filled"
                        value={this.state.dataPerfil.lastname}
                        error={this.hasError('lastname')}
                        helperText={this.displayError('lastname')}
                        disabled={this.state.disabledText.lastname}
                        InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <PersonAddIcon style={{ color: this.hasError('lastname')?red[500]:null}} /*color={this.hasError('lastname')?'error':''}*//>
                          </InputAdornment>
                        ),
                      }}
                        />

                    </Grid>
                    <Grid item md={12}>
                        <TextField
                        name="email"
                        label="Correo electrónico"
                        className={ classes.textField }
                        variant="filled"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        onKeyUp={this.handleKeyUp}
                        value={this.state.dataPerfil.email}
                        error={this.hasError('email')}
                        autoFocus
                        disabled={this.state.disabledText.email}
                        helperText={this.displayError('email')}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <MailOutlineIcon  style={{ color:this.hasError('email')?red[500]:null}} /*color={this.hasError('email')?'error':''}*//>
                            </InputAdornment>
                          ),
                          endAdornment:(
                            <InputAdornment position="end">
                              {this.state.dataPerfil.status===''?<SmsFailedOutlinedIcon style={{opacity:0,color:green[500]}}/>:this.state.dataPerfil.status===false?<SmsFailedOutlinedIcon  color="error"/>:
                              <VerifiedUserOutlinedIcon style={{color:green[500]}} />}
                            </InputAdornment>
                          ),
                        }}
                        />
                    </Grid>
                   <Grid item md={12} >
                    <Typography variant="h6" className={classes.title}>
                    Referidos
                    </Typography>
                        <Typography  >
                        <List >
                          <ListItem >
                            <ListItemText
                              primary={this.state.dataPerfil.referred}
                            />
                          </ListItem>
                      </List>
                      </Typography>
                    </Grid>

                    <Grid item  md={12}>
                    {this.state.wallets!==null && (
                    <Typography variant="h6" className={classes.title}>
                      Cuentas Asociadas
                    </Typography>)}
                    <div className={classes.dvd} id="listGeneral">
                    {this.state.wallets!==null && ( <List
                        component="nav"
                        aria-labelledby="nested-list-subheader"
                      >
                      {this.generate(
                          <ListItem >
                            <ListItemText
                              primary="Single-line item"
                            />
                          </ListItem>,
                      )}
                      </List>
                      )}
                    </div>

                    </Grid>

                    <Grid align="right" item md={12} className={ classes.grid }>
                        <Button onClick={clickPerfil} variant="outlined" color="secondary" >{this.state.buttonCreateUser===true?'Salir':'Cancelar'}</Button>
                        <Button disabled={this.isFormInvalid()===true?true:false} onClick={this.subscribe}  variant="outlined" color="primary">{this.state.existsEmail===null && this.state.wallets===null?"Crear":"Guardar"}</Button>
                    </Grid>
                </Grid>

            </div>
        </div>
    </Fade>
    )
  };
}
  export default withNamespaces()(withRouter(withStyles(styles)(FormPerfil)));
