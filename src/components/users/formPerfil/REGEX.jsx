 const REGEX ={
 NAMES_REGEX: new RegExp('^[a-zA-Z0-9_-]{4,16}$'),
 EMAIL_REGEX: new RegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
}
 export default REGEX;