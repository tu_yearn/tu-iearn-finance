// import { colors } from "../../theme";

import { red } from "@material-ui/core/colors";

const styles = (theme) => ({
  root: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    maxWidth: "1200px",
    width: "100%",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  intro: {
    width: "100%",
    marginTop: "20px",
    position: "relative",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column-reverse",
    },
  },
  title: {
    float: "left",
    color: "#3f51b5",
  },
  selectMenu: {
    width: "200px",
    marginBottom: "10px",
    marginRight: 10,
  },
  selectMenu2: {
    width: "400px",
    marginBottom: "10px",
    marginRight: 10,
  },
  loading: {
    position: "absolute",
    top: "50%",
    left: "50%",
  },
  space: {
    marginLeft: "10px",
    marginRight: "10px",
    marginBottom: "5px",
  },
  leyend: {
    color: "#3f51b5",
    float: "left",
    fontSize: "10px",
  },
  traderButton: {
    backgroundImage:
      "url(" + require("../../../assets/localbitcoins-logo.png").default + ")",
    backgroundSize: "100%",
    width: "20px",
    height: "20px",
  },
  cellRed: {
    color: red[700],
  },
  BoldCell: {
    fontWeight: 900,
  },
  upCase: {
    color: "black",
    textTransform: "uppercase",
  },
});

export default styles;
