import { colors } from '../../../theme'

const styles = theme => ({
    root: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      maxWidth: '1200px',
      width: '100%',
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    investedContainerLoggedOut: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      minWidth: '100%',
      marginTop: '40px',
      [theme.breakpoints.up('md')]: {
        minWidth: '900px',
      }
    },
    iHaveContainer: {
      flex: 1,
      display: 'flex',
      flexWrap: 'wrap',
      padding: '42px 30px',
      borderRadius: '50px',
      maxWidth: '500px',
      justifyContent: 'center',
      border: '1px solid '+colors.borderBlue,
    },
    iWantContainer: {
      flex: 1,
      display: 'flex',
      flexWrap: 'wrap',
      padding: '24px'
    },
    conversionRatioContainer: {
      width: '100%',
      display: 'flex'
    },
    sendingContainer: {
      flex: 1,
      display: 'flex',
    },
    receivingContainer: {
      flex: 1,
      display: 'flex',
    },
    feesContainer: {
      display: 'flex'
    },
    card: {
      width: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      maxWidth: '400px',
      justifyContent: 'center',
      minWidth: '100%',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: '40px'
    },
    intro: {
      width: '100%',
      position: 'relative',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingBottom: '32px',
      maxWidth: '500px'
    },
    actualIntro: {
      paddingBottom: '32px',
    },
    introCenter: {
      minWidth: '100%',
      textAlign: 'center',
      padding: '24px 0px'
    },
    investedContainer: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      padding: '12px',
      minWidth: '100%',
      [theme.breakpoints.up('md')]: {
        minWidth: '800px',
      }
    },
    connectContainer: {
      padding: '12px',
      display: 'flex',
      justifyContent: 'center',
      width: '100%',
      maxWidth: '450px',
      [theme.breakpoints.up('md')]: {
        width: '450',
      }
    },
    actionButton: {
      '&:hover': {
        backgroundColor: "#2F80ED",
      },
      padding: '12px',
      backgroundColor: "#2F80ED",
      borderRadius: '1rem',
      border: '1px solid #E1E1E1',
      fontWeight: 500,
      [theme.breakpoints.up('md')]: {
        padding: '15px',
      }
    },
    buttonText: {
      fontWeight: '700',
      color: 'white',
    },
    sepperator: {
      borderBottom: '1px solid #E1E1E1',
      minWidth: '100%',
      marginBottom: '24px',
      marginTop: '24px'
    },
    addressContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      overflow: 'hidden',
      flex: 1,
      whiteSpace: 'nowrap',
      fontSize: '0.83rem',
      textOverflow:'ellipsis',
      cursor: 'pointer',
      padding: '28px 30px',
      borderRadius: '50px',
      border: '1px solid '+colors.borderBlue,
      alignItems: 'center',
      [theme.breakpoints.up('md')]: {
        width: '100%'
      }
    },
    disaclaimer: {
      padding: '12px',
      border: '1px solid rgb(174, 174, 174)',
      borderRadius: '0.75rem',
      marginBottom: '24px',
      background: colors.white
    },
    walletAddress: {
      padding: '0px 12px'
    },
    walletTitle: {
      flex: 1,
      color: colors.darkGray
    },
    grey: {
      color: colors.darkGray
    },
  });

  export default styles;
