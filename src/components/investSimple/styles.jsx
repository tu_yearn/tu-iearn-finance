import { colors } from '../../theme'

const styles = theme => ({
    root: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      maxWidth: '1200px',
      width: '100%',
      justifyContent: 'flex-start',
      alignItems: 'center'
    },
    investedContainerLoggedOut: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      minWidth: '100%',
      marginTop: '40px',
      [theme.breakpoints.up('md')]: {
        minWidth: '900px',
      }
    },
    investedContainer: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'flex-start',
      minWidth: '100%',
      marginTop: '40px',
      [theme.breakpoints.up('md')]: {
        minWidth: '900px',
      }
    },
    balancesContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      flexWrap: 'wrap',
      justifyContent: 'flex-end',
      padding: '12px 12px',
      position: 'relative',
    },
    connectContainer: {
      padding: '12px',
      display: 'flex',
      justifyContent: 'center',
      width: '100%',
      maxWidth: '450px',
      [theme.breakpoints.up('md')]: {
        width: '450',
      }
    },
    intro: {
      width: '100%',
      position: 'relative',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      [theme.breakpoints.down('sm')]: {
        flexDirection: 'column-reverse',
      }
    },
    introCenter: {
      maxWidth: '500px',
      textAlign: 'center',
      display: 'flex',
      padding: '24px 0px'
    },
    introText: {
      paddingLeft: '20px'
    },
    actionButton: {
      '&:hover': {
        backgroundColor: "#2F80ED",
      },
      padding: '12px',
      backgroundColor: "#2F80ED",
      border: '1px solid #E1E1E1',
      fontWeight: 500,
      [theme.breakpoints.up('md')]: {
        padding: '15px',
      }
    },
    overlay: {
      position: 'absolute',
      borderRadius: '10px',
      background: 'RGBA(200, 200, 200, 1)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      border: '1px solid #aaa',
      cursor: 'pointer',

      right: '0px',
      top: '10px',
      height: '70px',
      width: '160px',
      [theme.breakpoints.up('md')]: {
        right: '0px',
        top: '10px',
        height: '90px',
        width: '210px',
      }
    },
    heading: {
      display: 'none',
      paddingTop: '12px',
      flex: 1,
      flexShrink: 0,
      [theme.breakpoints.up('sm')]: {
        paddingTop: '5px',
        display: 'block'
      }
    },
    headingName: {
      paddingTop: '5px',
      flex: 2,
      flexShrink: 0,
      display: 'flex',
      alignItems: 'center',
      minWidth: '100%',
      [theme.breakpoints.up('sm')]: {
        minWidth: 'auto',
      }
    },
    buttonText: {
      fontWeight: '700',
      color: 'white',
    },
    assetSummary: {
      display: 'flex',
      alignItems: 'center',
      flex: 1,
      flexWrap: 'wrap',
      [theme.breakpoints.up('sm')]: {
        flexWrap: 'nowrap'
      }
    },
    assetIcon: {
      display: 'flex',
      alignItems: 'center',
      verticalAlign: 'middle',
      borderRadius: '20px',
      height: '30px',
      width: '30px',
      textAlign: 'center',
      cursor: 'pointer',
      marginRight: '20px',
      [theme.breakpoints.up('sm')]: {
        height: '40px',
        width: '40px',
        marginRight: '24px',
      }
    },
    addressContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      overflow: 'hidden',
      flex: 1,
      whiteSpace: 'nowrap',
      fontSize: '0.83rem',
      textOverflow:'ellipsis',
      cursor: 'pointer',
      padding: '28px 30px',
      borderRadius: '50px',
      border: '1px solid '+colors.borderBlue,
      alignItems: 'center',
      maxWidth: 'calc(100vw - 24px)',
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '100%',
        maxWidth: 'auto'
      }
    },
    between: {
      width: '40px',
      height: '40px'
    },
    expansionPanel: {
      maxWidth: 'calc(100vw - 24px)',
      width: '100%'
    },
    versionToggle: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },
    tableHeadContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    investAllContainer: {
      paddingTop: '24px',
      display: 'flex',
      justifyContent: 'flex-end',
      width: '100%'
    },
    disaclaimer: {
      padding: '12px',
      border: '1px solid rgb(174, 174, 174)',
      borderRadius: '0.75rem',
      marginBottom: '24px',
      background: colors.white
    },
    walletAddress: {
      padding: '0px 12px'
    },
    walletTitle: {
      flex: 1,
      color: colors.darkGray
    },
    grey: {
      color: colors.darkGray
    },
    basedOnContainer: {
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
      alignItems: 'center'
    }
  });

  export default styles;
