import React, { Fragment, Component } from "react";
import { withRouter } from "react-router-dom";
import { withNamespaces } from "react-i18next";
import { withStyles } from "@material-ui/core/styles";
import {
  Button,
  CircularProgress,
  FormGroup,
  FormLabel,
  TextField,
} from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import { createTheme, MuiThemeProvider } from "@material-ui/core/styles";
import styles from "./styles";
import clsx from "clsx";
import { getCoingeckoMarkets } from "../../services/index";

class Coins extends Component {
  loadData = this.props.t("Alert.loadData");
  state = {
    page: 0,
    count: 1,
    rowsPerPage: 100,
    sortOrder: {},
    data: [[this.loadData]],
    isLoading: false,
    priceFilterChecked: false,
    filterSelect: "",
    textSearchFilter: "",
  };

  headerTable = () => {
    return {
      className: clsx({
        [this.props.classes.BoldCell]: true,
      }),
      style: {
        textDecoration: "underline",
      },
    };
  };

  convertToCurrency = (value) => {
    const nf = new Intl.NumberFormat("es-VE", {
      style: "currency",
      currency: "USD",
      minimumFractionDigits: 2,
      maximumFractionDigits: 6,
    });
    if (isNaN(value)) {
      return "";
    }
    return nf.format(value);
  };

  convertToPercent = (value) => {
    const nf = new Intl.NumberFormat("es-VE", {
      style: "percent",
      minimumFractionDigits: 1,
      maximumFractionDigits: 1,
    });
    if (isNaN(value)) {
      return "";
    }
    return (
      <span className={value < 0 ? this.props.classes.cellRed : null}>
        {nf.format(value / 100)}
      </span>
    );
  };

  getMuiTheme = () =>
    createTheme({
      overrides: {
        MUIDataTable: {
          root: {
            backgroundColor: "inherit",
          },
          paper: {
            boxShadow: "none",
          },
        },
        MUIDataTableBodyCell: {
          root: {
            backgroundColor: "none",
          },
        },
      },
    });

  componentDidMount() {
    this.setState({ isLoading: true });
    const param = {
      page: "1",
      limit: "100",
      sortOrderName: "market_cap",
      sortOrderDirection: "DESC",
      filter: "",
      textSearch: "",
    };
    getCoingeckoMarkets(param).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
      });
    });
  }

  componentWillUnmount() {
    this.setState = (state, callback) => {
      return;
    };
  }

  sort = (page, sortOrder) => {
    this.setState({ isLoading: true });
    const { rowsPerPage, filterSelect } = this.state;
    const param = {
      page: page + 1,
      limit: rowsPerPage,
      textSearch: "",
      sortOrderName: sortOrder.name ? sortOrder.name : "market_cap",
      sortOrderDirection: sortOrder.direction ? sortOrder.direction : "DESC",
      filter: filterSelect,
    };
    getCoingeckoMarkets(param).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
      });
    });
  };

  changePage = (page, sortOrder) => {
    this.setState({ isLoading: true });
    const { rowsPerPage, filterSelect, textSearchFilter } = this.state;
    const param = {
      page: page + 1,
      limit: rowsPerPage,
      textSearch: textSearchFilter,
      sortOrderName: sortOrder.name ? sortOrder.name : "market_cap",
      sortOrderDirection: sortOrder.direction ? sortOrder.direction : "DESC",
      filter: filterSelect,
    };
    getCoingeckoMarkets(param).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
      });
    });
  };

  changeRowsPerPage = (sortOrder, rowsPerPage) => {
    this.setState({ isLoading: true });
    const { filterSelect, textSearchFilter } = this.state;
    const param = {
      page: 1,
      limit: rowsPerPage,
      textSearch: textSearchFilter,
      sortOrderName: sortOrder.name ? sortOrder.name : "market_cap",
      sortOrderDirection: sortOrder.direction ? sortOrder.direction : "DESC",
      filter: filterSelect,
    };
    getCoingeckoMarkets(param).then((res) => {
      // console.log(res);
      this.setState({
        page: res.page,
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        rowsPerPage,
      });
    });
  };

  render() {
    const { data, count, isLoading, rowsPerPage, sortOrder } = this.state;
    const { classes, t } = this.props;

    const columns = [
      {
        name: "market_cap_rank",
        label: "#",
        options: {
          sort: true,
          filter: false,
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      {
        name: "image",
        label: "Icono",
        options: {
          sort: false,
          filter: false,
          setCellHeaderProps: () => ({
            style: { color: "white" },
          }),
          customBodyRenderLite: (value) => {
            return data[value].image ? (
              <img src={`${data[value].image}`} alt={data[value].id} />
            ) : (
              ""
            );
          },
        },
      },
      {
        name: "name",
        label: t("Coins.Coin"),
        options: {
          sort: true,
          filter: false,
          setCellHeaderProps: () => this.headerTable(),
          customBodyRenderLite: (value) => {
            return (
              <a
                href={`https://www.coingecko.com/es/monedas/${data[value].id}`}
                target="_blank"
                rel="noopener noreferrer"
                style={{ color: "black", fontWeight: 900 }}
              >
                {data[value].name}
              </a>
            );
          },
        },
      },
      {
        name: "symbol",
        label: t("Coins.Symbol"),
        options: {
          sort: false,
          filter: false,
          setCellHeaderProps: () => ({
            style: { color: "white" },
          }),
          setCellProps: () => ({
            className: clsx({
              [classes.upCase]: true,
            }),
          }),
        },
      },
      {
        name: "current_price",
        label: t("Bridge.Price"),
        options: {
          sort: true,
          filter: true,
          filterType: "custom",
          customFilterListOptions: {
            render: (v) =>
              this.renderCustomFilterListOptions(v, t("Bridge.Price")),
            update: (filterList, filterPos, index) =>
              this.updateCustomFilterListOptions(
                filterList,
                filterPos,
                index,
                columns
              ),
          },
          filterOptions: {
            names: [],
            logic(current_price, filters) {
              if (filters[0] && filters[1]) {
                return current_price < filters[0] || current_price > filters[1];
              } else if (filters[0]) {
                return current_price < filters[0];
              } else if (filters[1]) {
                return current_price > filters[1];
              }
              return false;
            },
            display: (filterList, onChange, index, column) =>
              this.displayfilterOptions(
                filterList,
                onChange,
                index,
                column,
                t("Bridge.Price")
              ),
          },
          setCellProps: () => ({ style: { textAlign: "end" } }),
          customBodyRenderLite: (value) =>
            this.convertToCurrency(data[value].current_price),
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      {
        name: "price_change_percentage_1h_in_currency",
        label: "1h",
        options: {
          sort: true,
          filter: true,
          filterType: "custom",
          customFilterListOptions: {
            render: (v) => this.renderCustomFilterListOptions(v, "% 1h"),
            update: (filterList, filterPos, index) =>
              this.updateCustomFilterListOptions(
                filterList,
                filterPos,
                index,
                columns
              ),
          },
          filterOptions: {
            names: [],
            logic(price_change_percentage_1h_in_currency, filters) {
              if (filters[0] && filters[1]) {
                return (
                  price_change_percentage_1h_in_currency < filters[0] ||
                  price_change_percentage_1h_in_currency > filters[1]
                );
              } else if (filters[0]) {
                return price_change_percentage_1h_in_currency < filters[0];
              } else if (filters[1]) {
                return price_change_percentage_1h_in_currency > filters[1];
              }
              return false;
            },
            display: (filterList, onChange, index, column) =>
              this.displayfilterOptions(
                filterList,
                onChange,
                index,
                column,
                "% 1h"
              ),
          },
          customBodyRenderLite: (value) =>
            this.convertToPercent(
              data[value].price_change_percentage_1h_in_currency
            ),
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      {
        name: "market_cap_change_percentage_24h",
        label: "24h",
        options: {
          sort: true,
          filter: true,
          filterType: "custom",
          customFilterListOptions: {
            render: (v) => this.renderCustomFilterListOptions(v, "% 24h"),
            update: (filterList, filterPos, index) =>
              this.updateCustomFilterListOptions(
                filterList,
                filterPos,
                index,
                columns
              ),
          },
          filterOptions: {
            names: [],
            logic(market_cap_change_percentage_24h, filters) {
              if (filters[0] && filters[1]) {
                return (
                  market_cap_change_percentage_24h < filters[0] ||
                  market_cap_change_percentage_24h > filters[1]
                );
              } else if (filters[0]) {
                return market_cap_change_percentage_24h < filters[0];
              } else if (filters[1]) {
                return market_cap_change_percentage_24h > filters[1];
              }
              return false;
            },
            display: (filterList, onChange, index, column) =>
              this.displayfilterOptions(
                filterList,
                onChange,
                index,
                column,
                "% 24h"
              ),
          },
          customBodyRenderLite: (value) =>
            this.convertToPercent(data[value].market_cap_change_percentage_24h),
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      {
        name: "price_change_percentage_7d_in_currency",
        label: "7d",
        options: {
          sort: true,
          filter: true,
          filterType: "custom",
          customFilterListOptions: {
            render: (v) => this.renderCustomFilterListOptions(v, "% 7d"),
            update: (filterList, filterPos, index) =>
              this.updateCustomFilterListOptions(
                filterList,
                filterPos,
                index,
                columns
              ),
          },
          filterOptions: {
            names: [],
            logic(price_change_percentage_7d_in_currency, filters) {
              if (filters[0] && filters[1]) {
                return (
                  price_change_percentage_7d_in_currency < filters[0] ||
                  price_change_percentage_7d_in_currency > filters[1]
                );
              } else if (filters[0]) {
                return price_change_percentage_7d_in_currency < filters[0];
              } else if (filters[1]) {
                return price_change_percentage_7d_in_currency > filters[1];
              }
              return false;
            },
            display: (filterList, onChange, index, column) =>
              this.displayfilterOptions(
                filterList,
                onChange,
                index,
                column,
                "% 7d"
              ),
          },
          customBodyRenderLite: (value) =>
            this.convertToPercent(
              data[value].price_change_percentage_7d_in_currency
            ),
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      {
        name: "total_volume",
        label: t("Coins.Volume24"),
        options: {
          sort: true,
          filter: true,
          filterType: "custom",
          customFilterListOptions: {
            render: (v) =>
              this.renderCustomFilterListOptions(v, t("Coins.Volume")),
            update: (filterList, filterPos, index) =>
              this.updateCustomFilterListOptions(
                filterList,
                filterPos,
                index,
                columns
              ),
          },
          filterOptions: {
            names: [],
            logic(total_volume, filters) {
              if (filters[0] && filters[1]) {
                return total_volume < filters[0] || total_volume > filters[1];
              } else if (filters[0]) {
                return total_volume < filters[0];
              } else if (filters[1]) {
                return total_volume > filters[1];
              }
              return false;
            },
            display: (filterList, onChange, index, column) =>
              this.displayfilterOptions(
                filterList,
                onChange,
                index,
                column,
                t("Coins.Volume")
              ),
          },
          setCellProps: () => ({ style: { textAlign: "end" } }),
          customBodyRenderLite: (value) =>
            this.convertToCurrency(data[value].total_volume),
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      {
        name: "market_cap",
        label: t("Coins.MktCap"),
        options: {
          filter: true,
          filterType: "custom",
          customFilterListOptions: {
            render: (v) =>
              this.renderCustomFilterListOptions(v, t("Coins.MktCap")),
            update: (filterList, filterPos, index) =>
              this.updateCustomFilterListOptions(
                filterList,
                filterPos,
                index,
                columns
              ),
          },
          filterOptions: {
            names: [],
            logic(market_cap, filters) {
              if (filters[0] && filters[1]) {
                return market_cap < filters[0] || market_cap > filters[1];
              } else if (filters[0]) {
                return market_cap < filters[0];
              } else if (filters[1]) {
                return market_cap > filters[1];
              }
              return false;
            },
            display: (filterList, onChange, index, column) =>
              this.displayfilterOptions(
                filterList,
                onChange,
                index,
                column,
                t("Coins.MktCap")
              ),
          },
          setCellProps: () => ({ style: { textAlign: "end" } }),
          customBodyRenderLite: (value) =>
            this.convertToCurrency(data[value].market_cap),
          setCellHeaderProps: () => this.headerTable(),
        },
      },
      /* {
        name: "total_volume",
        label: "Últimos 7 días",
        options: {
          sort: true,
          filter: false,
          display: false,
          setCellHeaderProps: () => this.headerTable(),
        },
      }, */
    ];

    const options = {
      filter: true,
      filterType: "dropdown",
      confirmFilters: true,
      // Calling the applyNewFilters parameter applies the selected filters to the table
      customFilterDialogFooter: (currentFilterList, applyNewFilters) => {
        return (
          <div style={{ marginTop: "40px" }}>
            <Button
              variant="contained"
              onClick={() => this.handleFilterSubmit(applyNewFilters, columns)}
            >
              {this.props.t("Coins.ApplyFilters")}
            </Button>
          </div>
        );
      },
      // callback that gets executed when filters are confirmed
      onFilterConfirm: (filterList) => {
        // console.log("onFilterConfirm");
        console.dir(filterList);
      },
      onFilterDialogOpen: () => {
        // console.log("filter dialog opened");
      },
      onFilterDialogClose: () => {
        // console.log("filter dialog closed");
      },
      onFilterChange: (column, filterList, type) => {
        if (type === "chip") {
          // console.log("updating filters via chip");
        }
      },

      searchProps: {
        onBlur: (e) => {
          this.textSearch(e.target.value);
        },
        onKeyUp: (e) => {
          if (e.keyCode === 13) {
            this.textSearch(e.target.value);
          }
        },
      },

      responsive: "standard",
      serverSide: true,
      count: count,
      rowsPerPage: rowsPerPage,
      rowsPerPageOptions: [10, 50, 100, 150, 250],
      sortOrder: sortOrder,
      selectableRows: "none",
      onTableChange: (action, tableState) => {
        // console.log(action, tableState);

        // a developer could react to change on an action basis or
        // examine the state as a whole and do whatever they want

        switch (action) {
          case "changePage":
            this.changePage(tableState.page, tableState.sortOrder);
            break;
          case "sort":
            this.sort(tableState.page, tableState.sortOrder);
            break;
          case "filterChange":
            // console.log(tableState);
            break;
          case "changeRowsPerPage":
            this.changeRowsPerPage(
              tableState.sortOrder,
              tableState.rowsPerPage
            );
            break;
          case "onSearchClose":
            this.setState({ textSearchFilter: "" });
            this.textSearch("");
            break;
          default:
          // console.log("action not handled.");
        }
      },
      textLabels: {
        pagination: {
          next: t("Bridge.Next"),
          previous: t("Bridge.Previous"),
          rowsPerPage: t("Bridge.RowsPerPage"),
          displayRows: t("Bridge.DisplayRows"),
        },
        toolbar: {
          search: t("Bridge.Search"),
          downloadCsv: t("Bridge.Download") + " CSV",
          print: t("Bridge.Print"),
          viewColumns: t("Bridge.Column"),
          filterTable: t("Bridge.Filter"),
        },
        filter: {
          all: t("Bridge.Alls"),
          title: t("Bridge.Filters"),
          reset: t("Bridge.Reset"),
        },
        viewColumns: {
          title: t("Bridge.ShowColumns"),
          titleAria: t("Bridge.ShowHideColumns"),
        },
      },
    };

    // console.log("COLUMNS");
    // console.dir(JSON.parse(JSON.stringify(columns)));

    return (
      <Fragment>
        {isLoading && (
          <CircularProgress size={80} className={classes.loading} />
        )}
        <div className={classes.root}>
          <div className={classes.space}>
            <MuiThemeProvider theme={this.getMuiTheme()}>
              <MUIDataTable
                title={
                  <div>
                    <h1 className={classes.title}>{t("Home.Coins")}</h1>
                  </div>
                }
                data={data}
                columns={columns}
                options={options}
              />
            </MuiThemeProvider>
          </div>
        </div>
      </Fragment>
    );
  }

  textSearch = (text) => {
    this.setState({
      isLoading: true,
      textSearchFilter: text,
      data: [[this.loadData]],
    });
    const { rowsPerPage, filterSelect, sortOrder } = this.state;
    const param = {
      page: 1,
      limit: rowsPerPage,
      textSearch: text,
      sortOrderName: sortOrder.name ? sortOrder.name : "market_cap",
      sortOrderDirection: sortOrder.direction ? sortOrder.direction : "DESC",
      filter: filterSelect,
    };
    getCoingeckoMarkets(param).then((res) => {
      // console.log(res);
      this.setState({
        page: res.page,
        data: res.data,
        isLoading: false,
        count: res.totalCount,
        rowsPerPage,
      });
    });
  };

  handleFilterSubmit = (applyFilters, columns) => {
    let filterList = applyFilters();
    const col = columns.map((elem) => elem.name);
    const fil = filterList.map((elem) => {
      if (elem[0] === "") elem[0] = undefined;
      if (elem[1] === "") elem[1] = undefined;
      if (elem[0] !== undefined && elem[1] !== undefined) {
        return `BETWEEN ${elem[0]} AND ${elem[1]}`;
      } else if (elem[0] !== undefined) {
        return `>= ${elem[0]}`;
      } else if (elem[1] !== undefined) {
        return `<= ${elem[1]}`;
      } else return "";
    });
    let cont = "";
    let i = 0;
    col.forEach((element, index) => {
      if (fil[index] !== "") {
        if (i !== 0) {
          cont = cont + " AND";
        }
        cont = cont + " " + element + " " + fil[index];
        // console.log(element, fil[index]);
        i++;
      }
    });
    this.setState({ isLoading: true, filterSelect: cont });
    const { page, rowsPerPage, textSearchFilter } = this.state;
    const param = {
      page: page + 1,
      limit: rowsPerPage,
      sortOrderName: "market_cap",
      sortOrderDirection: "DESC",
      filter: cont,
      textSearch: textSearchFilter,
    };
    getCoingeckoMarkets(param).then((res) => {
      // console.log(res);
      this.setState({
        data: res.data,
        isLoading: false,
        count: res.totalCount,
      });
    });
  };

  renderCustomFilterListOptions = (v, label) => {
    if (v[0] && v[1] && this.state.priceFilterChecked) {
      return [`Min ${label}: ${v[0]}`, `Max ${label}: ${v[1]}`];
    } else if (v[0] && v[1] && !this.state.priceFilterChecked) {
      return `Min ${label}: ${v[0]}, Max ${label}: ${v[1]}`;
    } else if (v[0]) {
      return `Min ${label}: ${v[0]}`;
    } else if (v[1]) {
      return `Max ${label}: ${v[1]}`;
    }
    return [];
  };

  updateCustomFilterListOptions = (filterList, filterPos, index, columns) => {
    // console.log("customFilterListOnDelete: ", filterList, filterPos, index);
    let newFilters = () => filterList;

    if (filterPos === 0) {
      filterList[index].splice(filterPos, 1, "");
    } else if (filterPos === 1) {
      filterList[index].splice(filterPos, 1);
    } else if (filterPos === -1) {
      filterList[index] = [];
    }

    this.handleFilterSubmit(newFilters, columns);

    return filterList;
  };

  displayfilterOptions = (filterList, onChange, index, column, title) => {
    return (
      <div>
        <FormLabel>{title}</FormLabel>
        <FormGroup row>
          <TextField
            label="min"
            value={filterList[index][0] || ""}
            onChange={(event) => {
              filterList[index][0] = event.target.value;
              onChange(filterList[index], index, column);
            }}
            style={{ width: "45%", marginRight: "5%" }}
          />
          <TextField
            label="max"
            value={filterList[index][1] || ""}
            onChange={(event) => {
              filterList[index][1] = event.target.value;
              onChange(filterList[index], index, column);
            }}
            style={{ width: "45%" }}
          />
        </FormGroup>
      </div>
    );
  };
}

export default withNamespaces()(withRouter(withStyles(styles)(Coins)));
