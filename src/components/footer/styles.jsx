import { colors } from '../../theme';

const styles = theme => ({
    footer: {
      padding: '24px',
      display: 'flex',
      justifyContent: 'space-evenly',
      width: '100%',
      background: '#0d6efd',
      color: colors.white,
      /* borderRadius: '50px 50px 0px 0px', */
      border: '1px solid '+colors.borderBlue,
      borderBottom: 'none',
      marginTop: '48px',
      flexWrap: 'wrap',
      /* [theme.breakpoints.down('xs')]: {
        justifyContent: 'flex-start',
      } */
    },
    heading: {
      marginBottom: '12px',
      paddingBottom: '9px',
      borderBottom: "3px solid "+colors.borderBlue,
      width: 'fit-content',
      marginLeft: '30px'
    },
    link: {
      paddingBottom: '12px',
      cursor: 'pointer',
      display: 'flex',
      alignItems: 'center',
      '&:hover': {
        textDecoration: 'underline'
      }
    },
    logo: {
        cursor: 'pointer',
        marginLeft: '6px',
        marginRight: '6px',
    },
    icon: {
      fill: colors.borderBlue,
      marginRight: '6px',
    },
    yearnIcon: {
      minHeight: '100%',
      display: 'flex',
      alignItems: 'center'
    },
    builtWith:{
      display: 'flex',
      /* flexDirection: 'column', */
      alignItems: 'center',
     /*  [theme.breakpoints.down('md')]: {
        display: 'none',
      } */
    },
    builtWithLink: {
      paddingTop: '12px'
    },
    builtHeading: {
      marginBottom: '12px',
      paddingBottom: '9px',
      borderBottom: "3px solid "+colors.borderBlue,
      width: 'fit-content',
      cursor: 'pointer'
    },
    products: {
      padding: '0px 24px',
     /*  [theme.breakpoints.down('xs')]: {
        paddingBottom: '24px'
      } */
    },
    community: {
      padding: '0px 24px',
      [theme.breakpoints.down('xs')]: {
        paddingBottom: '24px'
      }
    },
    socials: {
      padding: '0px 24px'
    },
    btnPay: {
        padding: '12px',
        border: '2px solid rgb(174, 174, 174)',
        borderRadius: '50px',
        display: 'flex',
        alignItems: 'center',
        cursor: 'pointer',
        '&:hover': {
          border: "2px solid "+colors.borderBlue,
          background: 'rgba(47, 128, 237, 0.1)'
        },
      },
  });

  export default styles;
