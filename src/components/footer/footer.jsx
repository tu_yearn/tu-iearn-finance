import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Slider
} from '@material-ui/core';

import { withNamespaces } from 'react-i18next';

import styles from "./styles";
/* import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import HowToVoteIcon from '@material-ui/icons/HowToVote';
import SecurityIcon from '@material-ui/icons/Security';
import DescriptionIcon from '@material-ui/icons/Description';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import ForumIcon from '@material-ui/icons/Forum';
import BarChartIcon from '@material-ui/icons/BarChart'; */
import BuildIcon from '@material-ui/icons/Build';

import BuiltWithModal from '../builtwith/builtwithModal.jsx'
import tufiLogo from '../../assets/TUFi-logo.png';

const PrettoSlider = withStyles({
    root: {
      color: '#52af77',
      height: 8,
    },
    thumb: {
      height: 24,
      width: 24,
      backgroundColor: '#fff',
      border: '2px solid currentColor',
      marginTop: -8,
      marginLeft: -12,
      '&:focus, &:hover, &$active': {
        boxShadow: 'inherit',
      },
    },
    active: {},
    valueLabel: {
      left: 'calc(-50% + 4px)',
    },
    track: {
      height: 8,
      borderRadius: 4,
    },
    rail: {
      height: 8,
      borderRadius: 4,
    },
  })(Slider);

class Footer extends Component {

  constructor(props) {
    super()

    this.state = {
      modalBuiltWithOpen: false,
      value: 20
    }
  }

  hangleChange = (event, newValue) => {
    this.setState({ value: newValue })
  }

  render() {
    const { classes, t, location } = this.props;
    const {
      modalBuiltWithOpen,
      value
    } = this.state;

    if(location.pathname === '' || location.pathname === '/') {
      return null
    }

    return (
      <div className={classes.footer}>
        <div className={ classes.builtWith }>
          <Typography className={ classes.builtHeading } variant={ 'h6'} onClick={ () => { this.nav('') } }>TUFi.finance</Typography>
          <img
            className={ classes.logo }
            alt=""
            src={ tufiLogo }
            height={ '40px' }
            width={ '40px' }
            onClick={ () => { this.nav('') } }
          />
          <div  className={ `${classes.link} ${classes.builtWithLink}` } onClick={ () => { this.builtWithOverlayClicked() } } >
            <BuildIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >{ t("Builtwith") }</Typography>
          </div>
        </div>
        <div className={ classes.products }>
          <Typography className={ classes.heading } variant={ 'h6'}>Contribuir con TUFi</Typography>
          <PrettoSlider
            valueLabelDisplay="auto"
            ariaLabel="pretto slider"
            defaultValue={value}
            min={1}
            onChange={this.hangleChange} />
            <div>
                <Typography
                variant={"h4"}
                className={classes.btnPay}
                noWrap
                onClick={() => { this.nav(`btc-pay/${value}`); }}
              >
                Donar {value}$ con TU BtcPay
              </Typography>
            </div>
        </div>
        {/* <div className={ classes.products }>
          <Typography className={ classes.heading } variant={ 'h6'}>Productos</Typography>
          <div  className={ classes.link } onClick={()=> window.open("https://yearn.finance", "_blank")} >
            <AttachMoneyIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >yearn.finance</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://ygov.finance", "_blank")} >
            <HowToVoteIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >ygov.finance</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://yinsure.finance", "_blank")} >
            <SecurityIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >yinsure.finance</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://yborrow.finance", "_blank")} >
            <MonetizationOnIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >yborrow.finance</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://docs.yearn.finance", "_blank")} >
            <DescriptionIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >docs.yearn.finance</Typography>
          </div>
        </div>
        <div className={ classes.community }>
          <Typography className={ classes.heading } variant={ 'h6'}>Community</Typography>
          <div  className={ classes.link } onClick={()=> window.open("https://ycosystem.info", "_blank")} >
            <DescriptionIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >ycosystem.info</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://www.learnyearn.finance", "_blank")}>
            <DescriptionIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >learnyearn.finance</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://stats.finance/yearn", "_blank")} >
            <BarChartIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >stats.finance</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://yieldfarming.info", "_blank")} >
            <BarChartIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >yieldfarming.info</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://feel-the-yearn.app", "_blank")} >
            <BarChartIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >feel-the-yearn.app</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://yearn.snapshot.page", "_blank")} >
            <ForumIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >yearn.snapshot.page</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://vaults.finance", "_blank")} >
            <AttachMoneyIcon height='15px' className={ classes.icon } />
            <Typography variant={ 'h4'} >vaults.finance</Typography>
          </div>
        </div>
        <div className={ classes.socials }>
          <Typography className={ classes.heading } variant={ 'h6'}>Socials</Typography>
          <div  className={ classes.link } onClick={()=> window.open("https://twitter.com/iearnfinance", "_blank")} >
            <img alt="" src={ require('../../assets/twitter.svg') } height='24px' className={ classes.icon } />
            <Typography variant={ 'h4'} >Twitter</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://medium.com/iearn", "_blank")} >
            <img alt="" src={ require('../../assets/medium.svg') } height='24px' className={ classes.icon } />
            <Typography variant={ 'h4'} >Medium</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("http://discord.yearn.finance", "_blank")} >
            <img alt="" src={ require('../../assets/discord.svg') } height='24px' className={ classes.icon } />
            <Typography variant={ 'h4'} >Discord</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://t.me/yearnfinance", "_blank")} >
            <img alt="" src={ require('../../assets/telegram.svg') } height='24px' className={ classes.icon } />
            <Typography variant={ 'h4'} >Telegram</Typography>
          </div>
          <div  className={ classes.link } onClick={()=> window.open("https://github.com/iearn-finance", "_blank")} >
            <img alt="" src={ require('../../assets/github.svg') } height='24px' className={ classes.icon } />
            <Typography variant={ 'h4'} >Github</Typography>
          </div>
        </div> */}
        { modalBuiltWithOpen && this.renderBuiltWithModal() }
      </div>
    )
  }

  nav = (screen) => {
    if(screen === 'cover') {
      window.open("https://yinsure.finance", "_blank")
      return
    }
    this.props.history.push('/'+screen)
  }

  renderBuiltWithModal = () => {
    return (
      <BuiltWithModal closeModal={ this.closeBuiltWithModal } modalOpen={ this.state.modalBuiltWithOpen } />
    )
  }

  builtWithOverlayClicked = () => {
    this.setState({ modalBuiltWithOpen: true })
  }

  closeBuiltWithModal = () => {
    this.setState({ modalBuiltWithOpen: false })
  }
}

export default withNamespaces()(withRouter(withStyles(styles)(Footer)));
