import { colors } from "../../theme/theme";

const styles = (theme) => ({
  root: {
    flex: 1,
    display: "flex",
    width: "100%",
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "column",
    [theme.breakpoints.up("sm")]: {
      flexDirection: "row",
    },
  },
  card: {
    flex: "1",
    height: "25vh",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    cursor: "pointer",
    borderRadius: "0px",
    transition: "background-color 0.2s linear",
    [theme.breakpoints.up("sm")]: {
      height: "100vh",
      minHeight: "50vh",
    },
  },
  earn: {
    backgroundColor: colors.white,
    "&:hover": {
      backgroundColor: colors.pink,
      "& .title": {
        color: colors.white,
      },
      "& .icon": {
        color: colors.white,
      },
      "& .description": {
        display: "block",
        color: colors.white,
        padding: "48px",
        textAlign: "center",
      },
    },
    "& .title": {
      color: colors.pink,
    },
    "& .icon": {
      color: colors.pink,
    },
    "& .description": {
      display: "none",
    },
  },
  zap: {
    backgroundColor: colors.white,
    "&:hover": {
      backgroundColor: colors.blue,
      "& .title": {
        color: colors.white,
      },
      "& .icon": {
        color: colors.white,
      },
      "& .description": {
        display: "block",
        color: colors.white,
        padding: "48px",
        textAlign: "center",
      },
    },
    "& .title": {
      color: colors.blue,
      display: "block",
    },
    "& .soon": {
      color: colors.blue,
      display: "none",
    },
    "& .icon": {
      color: colors.blue,
    },
    "& .description": {
      display: "none",
    },
  },
  apr: {
    backgroundColor: colors.white,
    "&:hover": {
      backgroundColor: colors.lightBlack,
      "& .title": {
        color: colors.white,
      },
      "& .icon": {
        color: colors.white,
      },
      "& .description": {
        display: "block",
        color: colors.white,
        padding: "48px",
        textAlign: "center",
      },
    },
    "& .title": {
      color: colors.lightBlack,
    },
    "& .icon": {
      color: colors.lightBlack,
    },
    "& .description": {
      display: "none",
    },
  },
  vault: {
    backgroundColor: colors.white,
    "&:hover": {
      backgroundColor: colors.tomato,
      "& .title": {
        color: colors.white,
      },
      "& .icon": {
        color: colors.white,
      },
      "& .description": {
        display: "block",
        color: colors.white,
        padding: "48px",
        textAlign: "center",
      },
    },
    "& .title": {
      color: colors.tomato,
    },
    "& .icon": {
      color: colors.tomato,
    },
    "& .description": {
      display: "none",
    },
  },
  cover: {
    backgroundColor: colors.white,
    "&:hover": {
      backgroundColor: colors.compoundGreen,
      "& .title": {
        color: colors.white,
      },
      "& .icon": {
        color: colors.white,
      },
      "& .description": {
        display: "block",
        color: colors.white,
        padding: "48px",
        textAlign: "center",
      },
    },
    "& .title": {
      color: colors.compoundGreen,
    },
    "& .icon": {
      color: colors.compoundGreen,
    },
    "& .description": {
      display: "none",
    },
  },
  title: {
    padding: "24px",
    paddingBottom: "0px",
    [theme.breakpoints.up("sm")]: {
      paddingBottom: "24px",
    },
  },
  icon: {
    fontSize: "60px",
    [theme.breakpoints.up("sm")]: {
      fontSize: "100px",
    },
  },
  link: {
    textDecoration: "none",
  },
});

export default styles;
