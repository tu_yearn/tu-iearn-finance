import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';

import { withNamespaces } from 'react-i18next';

import styles from './styles';

class Construccion extends Component {

  constructor(props) {
    super()

    this.state = {
    }
  }

  render() {
    const { classes, t } = this.props;

    return (

      <div className={ classes.root }>
        <div><h2>{t("Construccion.Principal")}</h2></div>
         {/* <div><p>{t("List.Description1")}</p></div>

         <div><p>{t("List.Description2")}</p></div> */}


      </div>

    )
  };

  nav = (screen) => {
    this.props.history.push(screen)
  }
}

export default withNamespaces()(withRouter(withStyles(styles)(Construccion)));
