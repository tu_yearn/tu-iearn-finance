import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import { withNamespaces } from 'react-i18next';
import IframeResizer from 'iframe-resizer-react';

import styles from './styles'

class BtcPay extends Component {

  constructor(props) {
    super()
  }

  render() {
    const { classes } = this.props;
    const price = this.props.match.params.price;
    const storeId = '87AQbyuGWhN5tj8H6YkNJUQk4BxFS48j4N1jev9ptgDJ';
    const cur = 'USD';
    const srcLink = `https://btcpay.tufi.finance/api/v1/invoices?storeId=${storeId}&price=${price}&currency=${cur}`
    return (
      <div className={ classes.root } style={{ width: '100%', height: '850px' }}>
        <IframeResizer
            heightCalculationMethod="lowestElement"
            src={srcLink}
            style={{ width: '100%', height: '850px' }}
        />
      </div>
    )
  };

  nav = (screen) => {
    this.props.history.push(screen)
  }
}

export default withNamespaces()(withRouter(withStyles(styles)(BtcPay)));
