const styles = (theme) => ({
  root: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    maxWidth: "1200px",
    width: "80%",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  intro: {
    width: "100%",
    marginTop: "20px",
    position: "relative",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column-reverse",
    },
  },
  title: {
    float: "left",
    color: "#3f51b5",
  },
  param: {
    color: "#3f51b5",
  },
});

export default styles;
