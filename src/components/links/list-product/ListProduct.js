import React, { Fragment, Component } from "react";
import { withRouter } from "react-router-dom";
import { withNamespaces } from "react-i18next";
import { withStyles } from "@material-ui/core/styles";

import { createTheme } from "@material-ui/core/styles";
import styles from "./styles";

class ListProduct extends Component {
  getMuiTheme = () =>
    createTheme({
      overrides: {
        MUIDataTable: {
          root: {
            backgroundColor: "inherit",
          },
          paper: {
            boxShadow: "none",
          },
        },
        MUIDataTableBodyCell: {
          root: {
            backgroundColor: "none",
          },
        },
      },
    });

  render() {
    const { classes, t } = this.props;
    return (
      <Fragment>
        <div className={classes.root}>
          <h2>{t("List.Title")}</h2>
          <p>{t("List.Description1")}</p>
          <p>{t("List.Description2")}</p>
          <h3>{t("Home.Bridge")} P2P</h3>
          <ul>
            <li>{t("List.User")}</li>
            <li>
              {t("List.SimbolCripto")}{" "}
              <code className={classes.param}>[BTC, ETH,...]</code>
            </li>
            <li>
              {t("List.SimbolFiat")}{" "}
              <code className={classes.param}>[USD, EUR,...]</code>
            </li>
            <li>
              {t("List.TypeOperation")}{" "}
              <code className={classes.param}>[buy, sell]</code>
            </li>
            <li>
              {t("List.CodeCountry")}{" "}
              <code className={classes.param}>[US,ES,...]</code>
            </li>
            <li>{t("Bridge.PaymentMethod")}.</li>
            <li>{t("List.PriceCrypto")}</li>
            <li>{t("Bridge.MinimumAmount")}</li>
            <li>{t("Bridge.MaximumAmount")}</li>
            <li>{t("List.Bank")}</li>
            <li>{t("Bridge.LastOnline")}</li>
          </ul>
          <h3>{t("Home.Bridge")} CEXs</h3>
          <ul>
            <li>
              {t("List.SimbolCripto")}{" "}
              <code className={classes.param}>[BTC, ETH,...]</code>
            </li>
            <li>
              {t("List.SimbolFiat")}{" "}
              <code className={classes.param}>[USD, EUR,...]</code>
            </li>
            <li>
              {t("List.CodeCountry")}{" "}
              <code className={classes.param}>[US,ES,...]</code>
            </li>
            <li>{t("List.Price")} crypto/fiat</li>
          </ul>
          <h3>{t("Home.Lending")}</h3>
          <ul>
            <li>
              {t("List.SimbolCripto")}{" "}
              <code className={classes.param}>[BTC, ETH,...]</code>
            </li>
            <li>{t("List.InvestmentRate")}</li>
            <li>{t("List.CreditRate")}</li>
          </ul>
          <p>{t("List.Footer")}</p>
        </div>
      </Fragment>
    );
  }
}

export default withNamespaces()(withRouter(withStyles(styles)(ListProduct)));
