const urlBase = process.env.REACT_APP_URL_BASE;
export const urlTufiAdmin = process.env.REACT_APP_URL_ADMIN;

const varEnv = {
  urlLocalbitcoins: `${urlBase}/api/v1/localbitcoins`,
  urlLocalCoinSwap: `${urlBase}/api/v1/localcoinswap`,
  urlGetIp: "https://ipapi.co/json/",
  urlGetPriceMarket:
    "https://localbitcoins.com/api/equation/max(bitstampusd_avg, bitfinexusd_avg)*USD_in_",
  urlCoingeckoMarkets: `${urlBase}/api/v1/coins`,
  urlAds: `${urlBase}/api/v1/ads`,
  urlPaymentMethods: `${urlBase}/api/v1/payment-methods`,
  urlUserSubscribePost: `${urlBase}/api/v1/users/subscribe`,
  urlStableCoinsCexs: `${urlBase}/api/v1/bridge-cexs`,
  urlUserWalletGet: `${urlBase}/api/v1/users/wallet`,
  urlCreateUserPost: `${urlBase}/api/v1/users`,
  urlUpdateUserPut: `${urlBase}/api/v1/users`,
  urlUserMailGet: `${urlBase}/api/v1/users/mail`,
  urlExchagesDexs: `${urlBase}/api/v1/exchanges`,
  urlBridgeFiatNft: `${urlBase}/api/v1/ntf-marketplace`,
  urlDefiTools: `${urlTufiAdmin}/defi-tools`,
  urlExchangeCexs: `${urlTufiAdmin}/exchange-cexes`,
  urlLightningNetTools: `${urlTufiAdmin}/net-lightning-tools`,
  urlAxieScholarship: `${urlTufiAdmin}/axie-scholarships`,
  urlPlays: `${urlTufiAdmin}/plays`,
  urlAxieTools: `${urlTufiAdmin}/axie-tools`,
  urlNewsCriptos: `${urlTufiAdmin}/news-criptos?_locale=`,
};
export default varEnv;
